FROM node:12 AS build-env

WORKDIR /usr/src/app

COPY package* /usr/src/app/
RUN npm i

ADD . /usr/src/app/

RUN ./node_modules/.bin/webpack
RUN sed "s/<!-- script -->/<script src=\x22\/assets\/bundle.js\x22><\/script>/g; s/<!-- style -->/<link rel=\x22stylesheet\x22 href=\x22\/assets\/bundle.css\x22\/>/g"  ./lib/index.html > build/index.html && \
    sed "s/<!-- script -->/<script src=\x22\/assets\/bundle.js\x22><\/script>/g; s/<!-- style -->/<link rel=\x22stylesheet\x22 href=\x22\/assets\/bundle.css\x22\/>/g"  ./lib/index.html > build/admin.html && \
    sed "s/<!-- script -->/<script src=\x22\/assets\/bundle.js\x22><\/script>/g; s/<!-- style -->/<link rel=\x22stylesheet\x22 href=\x22\/assets\/bundle.css\x22\/>/g"  ./lib/index.html > build/client_reg.html
COPY assets/ build/assets/

FROM nginx:alpine

COPY --from=build-env /usr/src/app/build/ /var/www/
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]