"use strict";

const
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    fs = require('fs'),
    path = require('path'),
    webpack = require('webpack'),
    buildPath = 'build/assets',
    publicPath = 'assets';


//noinspection JSUnresolvedFunction
module.exports = {
    entry: {
        bundle: "./prods/Envoy/app/index.js",
        client_reg: "./prods/Hermit/app/index.js",
        admin: "./prods/Ratmann/app/index.js"
    },
    output: {
        path: path.resolve(__dirname, buildPath),
        filename: '[name].js',
        publicPath: '/' + publicPath + '/',
    },
    plugins: [
        new webpack.NoErrorsPlugin(),
        new webpack.IgnorePlugin(/vertx/),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),
        new ExtractTextPlugin('[name].css', {allChunks: true}),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: false,
            mangle: false,
            drop_console: true,
            unsafe: true,
            warnings: false
        }),
        new webpack.optimize.DedupePlugin(),
        function () {
            this.plugin('done', function (stats) {
                fs.writeFileSync(
                    path.resolve(__dirname, 'stats.json'),
                    JSON.stringify(stats.toJson())
                );
            });
        }
    ],
    resolve: {
        alias: {
            assets: path.resolve(__dirname, 'assets')
        },
        extensions: ['', '.js']
    },
    module: {
        loaders: [
            {
                test: /\.s?css$/,
                loader: ExtractTextPlugin.extract('css!sass')
            },
            {
                exclude: /node_modules/,
                test: /\.js$/,
                loader: 'babel'
            },
            {
                test: /\.(jpg|png)$/,
                loader: 'file'
            },
            {
                test: /\.json$/,
                loader: 'json'
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file-loader?name=fonts/[name].[ext]'
            }
        ]
    },
    buildPath,
    publicPath,
    bail: true,
};