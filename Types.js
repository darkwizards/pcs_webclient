"use strict";
/**
 * Created by BANO.notIT on 25.02.17.
 */


/**
 * @namespace VerQ
 */

/**
 * @class User
 * @property {String} id
 * @property {Array.<String>} children
 * @property {String} status
 * @property {String} name
 * @property {Number} type
 * @property {String} mail
 */