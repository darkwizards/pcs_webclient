/**
 * Created by banonotit on 25.12.16.
 */
import Manager from "../../../../appComponents/modules/CacheManager";


export default new Manager({
    fetch: "getAllMeetings",
    create: "createMeeting",
    get: "getMeeting",
    edit: "editMeeting",
    remove: "removeMeeting"
}, 9e5)