/**
 * Created by banonotit on 25.12.16.
 */
import Manager from "../../../../appComponents/modules/CacheManager";


export default new Manager({
    fetch: "getUsers",
    create: "createUser",
    get: "getUser",
    edit: "editUser",
    remove: "removeUser"
}, Infinity)