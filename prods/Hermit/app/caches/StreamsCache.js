/**
 * Created by banonotit on 22.12.16.
 */
import Store from "../../../../appComponents/modules/CacheManager";

export default new Store({
    create: 'createStream',
    get: 'getStream',
    fetch: 'getAllStreams',
    edit: 'editStream',
    remove: 'removeStream'
}, 18e5)


/**
 * @typedef {Object} MyStream
 * @property {string} id
 * @property {number} duration
 * @property {number} time_start
 * @property {string} owner_id
 */