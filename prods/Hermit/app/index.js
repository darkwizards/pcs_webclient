import React, {Component} from "react";
import ReactDOM from "react-dom";
import configureStore from "./store/configureStore";
import {Provider} from "react-redux";
import App from "../../../appComponents/components/App/App";
import {fetchAll as users} from "../../../appComponents/actions/users";
import {fetchAll as streams} from "../../../appComponents/actions/streams";
import fetcher from "../../../appComponents/modules/AutoFetcher";
import {SESSION_RESUMPTION} from "../../../appComponents/actions/profile";
import {setStore} from "../../../appComponents/modules/ErrorHandler";
import FastUser from "./containers/FastUser/FastUser";


// if (!window.Promise)
window.Promise = require("promise-polyfill");


const
    initialState = {},
    store = configureStore(initialState);


setStore(store);


store.dispatch({
    type: SESSION_RESUMPTION
});


fetcher('register', users);
fetcher('register', streams);
fetcher('set store', store);
fetcher('start');
fetcher();


class Root extends Component {

    render() {

        return (
            <Provider store={store}>
                <App>
                    <FastUser/>
                </App>
            </Provider>
        )

    }

}


let rootElement = document.getElementById('root');


ReactDOM.render(
    <Root />, rootElement
);