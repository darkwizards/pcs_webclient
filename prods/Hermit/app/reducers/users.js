/**
 * Created by banonotit on 27.11.16.
 */
import {ADD_USER, DEL_USER, UPDATE_QUE, RESTART_USERS} from "../../../../appComponents/actions/users";
import objectsEqual from "lodash/isEqual";


const defaultState = [];


export default function (state = defaultState, action) {

    switch (action.type) {
        case RESTART_USERS:

            return defaultState.slice();

        case ADD_USER:

            return state.slice().concat({
                ...action.data
            });

        case DEL_USER:

            return state.filter(el => !objectsEqual(el, {...el, ...action.data}));

        case UPDATE_QUE:

            return state.map(el => el.id != action.data.id ? el : {...el, ...action.data});

        default:

            return state;

    }

}