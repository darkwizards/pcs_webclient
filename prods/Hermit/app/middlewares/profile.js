import {LOGIN_RESPONSE, LOGOUT, USER_DATA_RECIVE, SESSION_RESUMPTION} from "../actions/profile";
import CookieStore from "../../../../appComponents/modules/CookieStore";
import cache from "../caches/UsersCache";
import fetcher from "../../../../appComponents/modules/AutoFetcher";
import {notify} from "../../../../appComponents/actions/notice";


function getUserData(id, token, dispatch) {

    cache
        .get(id, token)
        .then(function (data) {

            if (
                !'type_name_children_status_mail_id'
                    .split('_')
                    .every(el => data.hasOwnProperty(el))
            )
                return Promise.reject({statusText: 'server returned bad data'});

            // if (data.type > 0)
            //     return Promise.reject(window.location = "/");

            dispatch({
                type: USER_DATA_RECIVE,
                data
            });

            dispatch(notify({
                text: "Вы успешно вошли в систему",
                type: 1
            }));

            fetcher();

        })
        .catch(function (err) {

            dispatch(notify({text: err.statusText}));

            console.dir(err);

            return Promise.reject(err)

        });
}

let ThisStore = new CookieStore();

export default store => next => action => {
    switch (action.type) {
        case LOGIN_RESPONSE:

            ThisStore.setItem('session', action.data.id + '|' + action.data.token);

            next(action);

            getUserData(action.data.id, action.data.token, next);

            break;

        case SESSION_RESUMPTION:

            let ses = ThisStore.getItem('session');

            if (ses === undefined)
                return;

            let [id, token] = ses.split('|');

            next({type: LOGIN_RESPONSE, data: {id, token}});

            getUserData(id, token, next);

            break;

        case LOGOUT:

            ThisStore.removeItem('session');

            next(action);

            break;

        default:

            next(action);

            break;

    }
}