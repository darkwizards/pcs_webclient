"use strict";
/**
 * Created by BANO.notIT on 13.02.17.
 */
import {
    ADD_STREAM,
    REQUEST_STREAM_CREATION,
    REQUEST_STREAM_FETCH,
    REMOVE_STREAM,
    EDIT_STREAM,
    REQUEST_STREAM_EDIT,
    REQUEST_STREAM_REM
} from "../../../../appComponents/actions/streams";
import Streams from "../caches/StreamsCache";
import {notify} from "../../../../appComponents/actions/notice";
import Diff from "../../../../appComponents/modules/Diff";
import Handel from "../../../../appComponents/modules/ErrorHandler";


export default store => dispatch => action => {

    let state = store.getState();

    switch (action.type) {
        case REQUEST_STREAM_CREATION:
            Streams.create(action.data, state.session.token)
                .then(function (data) {

                    store.dispatch({
                        type: ADD_STREAM,
                        data
                    });

                    dispatch(notify({
                        type: 0,
                        text: "Поток успешно создан"
                    }))

                })
                .catch(function (error) {

                    dispatch(notify({
                        type: 3,
                        text: "Поток не создан"
                    }));

                    console.log(error);

                    return Handel(error, store.dispatch)

                });


            break;

        case REQUEST_STREAM_FETCH:

            if (state.session.user && ~state.session.user.id)
                Streams.fetch(state.session.token)
                    .then(/** @param {[]} streams */function (streams) {

                        let diff = Diff(state.streams, streams);

                        diff.deleted.forEach(function (data) {

                            store.dispatch({
                                type: REMOVE_STREAM,
                                data
                            })

                        });

                        diff.created.forEach(function (data) {

                            store.dispatch({
                                type: ADD_STREAM,
                                data
                            })

                        });

                        diff.changed.forEach(function (data) {

                            store.dispatch({
                                type: EDIT_STREAM,
                                data
                            })

                        })

                    })
                    .catch(function (error) {

                        dispatch(notify({
                            type: 3,
                            text: "Ошибка получения списка потоков"
                        }));

                        return Handel(error, dispatch)

                    });

            break;

        case REQUEST_STREAM_EDIT:

            if (state.session.user && ~state.session.user.id)
                Streams.edit(action.data.id, action.data, state.session.token)
                    .then(function (data) {

                        store.dispatch({
                            type: EDIT_STREAM,
                            data
                        });

                        dispatch(notify({
                            type: 1,
                            text: "Изменения успешно внесены"
                        }));

                    })
                    .catch(function (error) {

                        console.log(error);

                        dispatch(notify({
                            type: 3,
                            text: "Изменения не были внесены"
                        }));

                        return Handel(error, store.dispatch)

                    });

            break;

        case REQUEST_STREAM_REM:

            if (state.session.user && ~state.session.user.id)
                Streams.remove(action.data.id, state.session.token)
                    .then(function () {

                        dispatch(notify({
                            text: "Очередь удалена"
                        }));

                        dispatch({
                            type: REMOVE_STREAM,
                            data: action.data
                        });


                    })
                    .catch(function (error) {

                        console.log(error);

                        dispatch(notify({
                            type: 3,
                            text: "Очередь не была удалена"
                        }));

                        return Handel(error, store.dispatch)

                    });

            break;

        default:

            dispatch(action);

            break;

    }
}