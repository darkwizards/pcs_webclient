import React, {Component, PropTypes} from "react";
import Login from "../../../../../appComponents/components/RequireLogin/RequireLogin";
import Nav from "react-bootstrap/lib/Nav";
import NavItem from "react-bootstrap/lib/NavItem";
import Stream from "../../components/StreamItem/StreamItem";
import Meets from "../../caches/MeetingsCache";
import qs from "querystring";
import Classes from "../../../../../config/select_options/classes.json";
import sortBy from "lodash/sortBy";
import Select from "../../../../../appComponents/components/react-select";
import "../../../../../appComponents/components/react-select/scss/default.scss";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {notify} from "../../../../../appComponents/actions/notice";
import MeetItem from "../../components/MeetItem/MeetItem";
import RusEnd from "../../../../../appComponents/modules/RusNum";


const
    defaultState = {
        step: "0",
        name: "",
        user: {},
        streams: [],
        meets: [],
        filtred: [],
        processing: false,
        printed: false
    };

class FastUser extends Component {
    constructor(props) {

        super(props);

        this.state = defaultState;

        this.accept = this.accept.bind(this);

    }

    accept() {


        let ses = this.props.ses;

        this.setState({processing: true});

        Meets._req.post("/createTemporaryUser", qs.stringify({token: ses.token, name: this.state.name}))
            .then(({data}) => {
                this.setState({
                    user: data
                })
            })
            .catch(err => {

                console.log(err);

                this.props.notify({
                    type: 3,
                    text: "Ошибка создания пользователя"
                })

            })
            .then(() => {

                Promise.all(
                    this.state.streams.map(stream =>
                        Meets
                            .create({
                                owner_id: this.state.user.id,
                                min_time: +new Date(),
                                stream_id: stream.id
                            }, ses.token)
                            .then((meet) => {

                                this.setState(a => ({meets: [...a.meets, meet]}));

                                return meet

                            })
                    )
                )
                    .then(() => this.setState({step: "3"}))
                    .catch(err => {

                        console.log(err);

                        this.props.notify({type: 3, text: "Ошибка создания встреч"})

                    })
            })

    }


    render() {

        let
            Content = {
                "0": (
                    <div className="row text-center">
                        <div className="col-xs-12">
                            <h3>Запись на приём к учителям</h3>
                            <form
                                className="col-md-8 col-md-push-2 col-xs-12 login-form text-center"
                                onSubmit={ev => {

                                    ev.preventDefault();

                                    if (this.state.name.length >= 3)
                                        this.setState({step: "1"})

                                }}
                            >
                                <div className="control-group">
                                    <label required className="h6 control-label">Введите ФИО</label>
                                    <input
                                        placeholder="Иштар Артемий Олегович"
                                        type="text" className="form-control"
                                        value={this.state.name}
                                        onChange={ev => {
                                            this.setState({name: ev.target.value})
                                        }}
                                    />
                                </div>
                                <button
                                    autoFocus disabled={this.state.name.length < 3}
                                    className="btn-block btn btn-lg btn-success"
                                >
                                    Следующий шаг
                                </button>
                            </form>
                        </div>
                    </div>
                ),
                "1": (
                    <div className="row">
                        <h3 className="text-center">Учителя, принимающие сегодня</h3>
                        <div className="col-md-8 col-md-push-2 col-xs-12">
                            <div className="form-group login-form">
                                <div className="control-group">
                                    <label className="control-label h6 text-center">
                                        Введите классы, в которых учатся Ваши дети
                                    </label>
                                    <Select
                                        placeholder="5Б, 10А"
                                        multi
                                        value={this.state.classes}
                                        onChange={val => this.setState({filtred: val})}
                                        options={Classes}
                                    />
                                </div>
                                <div className="control-group">
                                    {
                                        this.state.streams.length == 0 ?
                                            <h6 className="h6 text-center text-info">
                                                Укажите учителей и перейдите на следующий
                                                шаг
                                            </h6> :
                                            <button

                                                className="btn btn-lg btn-success btn-block"
                                                onClick={() => this.setState({step: "2"})}
                                                disabled={!this.state.streams.length}
                                            >
                                                Следующий шаг
                                            </button>

                                    }
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12">
                            {
                                this.props.streams.map(str => (
                                    <Stream
                                        filtred={this.state.classes}
                                        onSubscribe={() => this.setState(a => ({streams: [...a.streams, str]}))}
                                        onUnsubscribe={() => this.setState(a => ({streams: a.streams.filter(a => a.id !== str.id)}))}
                                        key={str.id}
                                        subscribed={this.state.streams.some(id => str.id == id.id)}
                                        stream={str}
                                    />
                                ))
                            }
                        </div>
                    </div>
                ),
                "2": (
                    <div className="row">
                        <h3 className="text-center">
                            Подтверждение данных
                        </h3>
                        <div className="col-xs-12 col-md-8 col-md-push-2 login-form form-group">
                            <p className="h6">
                                Вас зовут {this.state.name}
                            </p>
                            <p className="h6">
                                Вы хотите встретиться с {
                                this.state.streams.length
                            } {
                                RusEnd("учител_ем_ями_ями", this.state.streams.length)
                            }
                            </p>
                            <p className="h6">
                                {RusEnd("Создан_а_ы_о", this.state.meets.length)} {this.state.meets.length} {RusEnd("встреч_а_и_", this.state.meets.length)}
                            </p>

                            <button
                                disabled={this.state.processing}
                                onClick={this.accept}
                                className="btn btn-info btn-lg btn-block"
                            >
                                Записаться
                            </button>
                        </div>
                    </div>
                ),
                "3": (
                    <div>
                        <h3 className="text-center hidden-print">Печать конечных данных</h3>
                        <div
                            className="btn-group-lg col-xs-12 col-md-push-2 col-md-8 hidden-print login-form"
                        >
                            <button className=" btn btn-success btn-block" onClick={() => {
                                window.print();
                                this.setState({printed: true})
                            }}>
                                <i className="glyphicon glyphicon-print"/> Распечатать
                            </button>
                            {
                                this.state.printed ?
                                    <button
                                        onClick={() => this.setState(defaultState)}
                                        className="btn-info btn  btn-block"
                                    >
                                        <i className="glyphicon glyphicon-repeat"/> Заново
                                    </button> :
                                    null
                            }
                        </div>

                        <div className="col-xs-12">
                            <h5 className="text-center">{this.state.user.name}</h5>
                            <table className="table table-bordered">
                                {
                                    sortBy(this.state.meets, "time_start").map(a => <MeetItem meet={a}/>)
                                }
                            </table>
                        </div>
                    </div>
                )
            };

        return (
            <Login>
                <div className="col-xs-12">
                    <Nav
                        className="hidden-print"
                        justified
                        bsStyle="tabs"
                        activeKey={this.state.step}
                        onSelect={step => this.setState({step})}
                    >
                        <NavItem eventKey="0" disabled={!!this.state.meets.length}>
                            №1 ФИО
                        </NavItem>
                        <NavItem eventKey="1"
                                 disabled={this.state.name.length < 3 || this.state.user.name || !!this.state.meets.length}
                        >
                            №2 Учителя
                        </NavItem>
                        <NavItem
                            eventKey="2"
                            disabled={!this.state.streams.length || this.state.name.length < 3 || !!this.state.meets.length}
                        >
                            №3 Подтверждение
                        </NavItem>
                        <NavItem
                            eventKey="3"
                            disabled={!this.state.meets.length}
                        >
                            №4 Печать
                        </NavItem>
                    </Nav>
                    {
                        Content[this.state.step]
                    }
                </div>
            </Login>
        )
    }
}

FastUser.contextTypes = {
    store: PropTypes.object
};

FastUser.propsTypes = {};

function mapStateToProps(state) {

    return {
        ses: state.session,
        streams: state.streams,
        router: state.router
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        notify
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FastUser);