import React, {Component, PropTypes} from "react";
import Users from "../../caches/UsersCache";
import Streams from "../../caches/StreamsCache";


export default class MeetItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            teacher: null,
            stream: null
        }


    }

    componentDidMount() {

        let token = this.context.store.getState().session.token;

        Users.get(this.props.meet.recipient_id, token)
            .then(teacher => this.setState({teacher}))
            .catch(err => {
                console.log(err);
                alert("Не удалось получить данные об учителе")
            });

        Streams.get(this.props.meet.stream_id, token)
            .then(stream => this.setState({stream}))
            .catch(err => {
                console.log(err);
                alert("Не удалось получить данные об очереди")
            })
    }

    render() {
        return (
            <tr>
                <td>
                    <b>
                        {
                            this.state.teacher ?
                                <span>{this.state.teacher.name} [{this.state.teacher.status}]</span> :
                                "Загрузка..."
                        }
                    </b>
                    <div>
                        {
                            this.state.stream ?
                                this.state.stream.description :
                                "Загрузка..."
                        }
                    </div>
                </td>
                <td className="text-center">
                    <div className="h6">
                        {
                            (new Date(+this.props.meet.time_start) + "").match(/\d{2}:\d{2}/)[0]
                        }
                    </div>
                </td>
            </tr>
        )
    }
}

MeetItem.contextTypes = {
    store: PropTypes.object
};

MeetItem.propsTypes = {};