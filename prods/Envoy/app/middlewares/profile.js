import {LOGIN_RESPONSE, LOGOUT, USER_DATA_RECIVE, SESSION_RESUMPTION} from "../../../../appComponents/actions/profile";
import {RESTART_STREAM} from "../../../../appComponents/actions/streams";
import {RESTART_MEETS} from "../../../../appComponents/actions/meets";
import CookieStore from "../../../../appComponents/modules/CookieStore";
import cache from "../caches/UsersCache";
import fetcher from "../../../../appComponents/modules/AutoFetcher";
import {notify} from "../../../../appComponents/actions/notice";
import {
    CREATE_SUBSCRIBE,
    CREATE_STREAM,
    VIEW_QUEUE,
    VIEW_USER,
    VIEW_STREAM,
    VIEW_SUBSCRIBE,
    EDIT_STREAM,
    DELETE_STREAM,
    EDIT_QUEUE,
    DELETE_SUBSCRIBE
} from "../../../../config/permissions.json";


function getUserData(id, token, dispatch) {

    const
        viewAll = VIEW_QUEUE | VIEW_SUBSCRIBE | VIEW_STREAM | VIEW_USER,
        _parent = viewAll | CREATE_SUBSCRIBE | DELETE_SUBSCRIBE,
        _teacher = viewAll | CREATE_STREAM | EDIT_STREAM | EDIT_QUEUE | DELETE_STREAM,
        perms = [
            _teacher | DELETE_SUBSCRIBE, // admin
            _parent,
            _teacher
        ];

    cache
        .get(id, token)
        .then(function (data) {

            if (
                !'type_name_children_status_mail_id'
                    .split('_')
                    .every(el => data.hasOwnProperty(el))
            )
                return Promise.reject({statusText: 'server returned bad data'});

            dispatch({
                type: USER_DATA_RECIVE,
                data: {
                    ...data,
                    perm: perms[data.type]

                }
            });

            dispatch(notify({
                text: "Вы успешно вошли в систему",
                type: 1
            }));

            fetcher();

        })
        .catch(function (err) {

            console.dir(err);

            return Promise.reject(err)

        });
}

let MyCookieStore = new CookieStore();

export default store => next => action => {
    switch (action.type) {
        case LOGIN_RESPONSE:

            MyCookieStore.setItem('session', action.data.id + '|' + action.data.token);

            next(action);

            getUserData(action.data.id, action.data.token, next);

            break;

        case SESSION_RESUMPTION:

            let ses = MyCookieStore.getItem('session');

            if (ses === undefined)
                return;

            let [id, token] = ses.split('|');

            next({type: LOGIN_RESPONSE, data: {id, token}});

            getUserData(id, token, next);

            break;

        case LOGOUT:

            MyCookieStore.removeItem('session');

            next({type: RESTART_STREAM});
            next({type: RESTART_MEETS});
            next(action);

            break;

        default:

            next(action);

            break;

    }
}