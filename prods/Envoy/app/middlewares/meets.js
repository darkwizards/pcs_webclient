"use strict";
/**
 * Created by BANO.notIT on 13.02.17.
 */
import {
    ADD_MEET,
    REQ_DEL_MEET,
    DEL_MEET,
    EDIT_MEET,
    REQ_EDIT_MEET,
    REQ_FETCH_MEETS,
    REQ_ADD_MEET,
    REQ_START_MEET,
    REQ_END_MEET,
    fetchAll
} from "../../../../appComponents/actions/meets";
import Meets from "../caches/MeetingsCache";
import {notify} from "../../../../appComponents/actions/notice";
import Diff from "../../../../appComponents/modules/Diff";
import count from "../../../../appComponents/modules/RusNum";
import Handel from "../../../../appComponents/modules/ErrorHandler";
import qs from "querystring";


export default store => next => action => {

    let state = store.getState();

    switch (action.type) {
        case REQ_ADD_MEET:
            Meets.create({...action.data, owner_id: state.session.user.id}, state.session.token)
                .then(function (data) {

                    next({
                        type: ADD_MEET,
                        data
                    });

                    store.dispatch(notify({
                        type: 0,
                        text: "Встреча назначена"
                    }))

                })
                .catch(function (error) {

                    store.dispatch(notify({
                        type: 3,
                        text: "Встреча не назначена"
                    }));

                    console.log(error);

                    return Handel(error, store.dispatch)

                });

            break;

        case REQ_FETCH_MEETS:

            if (state.session.user && ~state.session.user.id)
                Meets.fetch(state.session.token)
                    .then(/** @param {Array} data */function (data) {

                        let diff = Diff(state.meets, data);

                        diff.deleted.forEach(function (data) {

                            next({
                                type: DEL_MEET,
                                data
                            })

                        });

                        diff.created.forEach(function (data) {

                            next({
                                type: ADD_MEET,
                                data
                            })

                        });

                        diff.changed.forEach(function (data) {

                            next({
                                type: EDIT_MEET,
                                data
                            })

                        });

                        if (diff.deleted.length + diff.created.length + diff.changed.length)
                            store.dispatch(notify({
                                type: 0,
                                text: [

                                    diff.deleted.length ?
                                        count("Отменил_ась_лись_лись ", diff.deleted.length) +
                                        diff.deleted.length + count(" встреч_а_и_", diff.deleted.length) :

                                        null,

                                    diff.created.length ?
                                        diff.created.length + count(" нов_ая_ые_ых", diff.created.length) +
                                        count(" встреч_а_и_", diff.created.length) :

                                        null,

                                    diff.changed.length ?
                                        diff.changed.length + count(" встреч_а_и_", diff.changed.length) +
                                        count(" измен_ена_ены_ены", diff.changed.length) :

                                        null

                                ].filter(el => el).join(" ")
                            }))

                    })
                    .catch(function (error) {

                        console.dir(error);

                        store.dispatch(notify({
                            type: 3,
                            text: "Ошибка получения списка встреч"
                        }));

                        return Handel(error, store.dispatch);

                    });

            break;

        case REQ_EDIT_MEET:

            if (state.session.user && ~state.session.user.id)
                Meets.edit(action.data.id, action.data, state.session.token)
                    .then(function (data) {

                        next({
                            type: EDIT_MEET,
                            data
                        });

                        store.dispatch(notify({
                            type: 1,
                            text: "Изменения успешно внесены"
                        }));

                    })
                    .catch(function (error) {

                        console.log(error);

                        store.dispatch(notify({
                            type: 3,
                            text: "Изменения не были внесены"
                        }));

                        return Handel(error, next)

                    });

            break;

        case REQ_DEL_MEET:

            console.log(action.data);

            if (state.session.user && ~state.session.user.id)
                Meets.remove(action.data, state.session.token)
                    .then(function () {

                        next({
                            type: DEL_MEET,
                            data: {
                                id: action.data
                            }
                        });

                        store.dispatch(notify({
                            text: "Встреча отменена"
                        }))

                    })
                    .catch(function (error) {

                        console.log(error);

                        store.dispatch(notify({
                            type: 3,
                            text: "Встреча не была отменена"
                        }));

                        return Handel(error, store.dispatch)

                    });

            break;

        case REQ_START_MEET:

            if (state.session.user && ~state.session.user.id)
                Meets._req.post("startMeeting", qs.stringify({id: action.data, token: state.session.token}))
                    .then(function ({data}) {

                        store.dispatch(notify({
                            text: "Всреча начата"
                        }));
                        store.dispatch({
                            type: EDIT_MEET,
                            data
                        })

                    })
                    .catch(function (error) {

                        console.dir(error);

                        store.dispatch(notify({
                            text: "Встреча не начата",
                            type: 3
                        }));

                        return Handel(error, store.dispatch);

                    });
            break;

        case REQ_END_MEET:

            if (state.session.user && ~state.session.user.id)
                Meets._req.post("endMeeting", qs.stringify({id: action.data, token: state.session.token}))
                    .then(function ({data}) {
                        "use strict";

                        console.log(data);

                        store.dispatch({
                            type: EDIT_MEET,
                            data
                        });
                        store.dispatch(fetchAll())
                        store.dispatch(notify({
                            text: "Встреча закончена"
                        }));

                    })
                    .catch(function (error) {

                        console.dir(error);

                        store.dispatch(notify({
                            text: "Встреча не закончена",
                            type: 3
                        }));

                        return Handel(error, store.dispatch);

                    });
            break;

        default:

            next(action);

            break;

    }
}
