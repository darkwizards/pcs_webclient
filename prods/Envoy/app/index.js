import React, {Component} from "react";
import ReactDOM from "react-dom";
import configureStore from "./store/configureStore";
import {Provider} from "react-redux";
import AppRouter from "./routes";
import fetcher from "../../../appComponents/modules/AutoFetcher";
import {fetchAll as meets} from "../../../appComponents/actions/meets";
import {fetchAll as streams} from "../../../appComponents/actions/streams";
import {SESSION_RESUMPTION} from "../../../appComponents/actions/profile";
import {setStore as errorHendlerStoreSet} from "../../../appComponents/modules/ErrorHandler";


// if (!window.Promise)
window.Promise = require("promise-polyfill");


const
    initialState = {},
    store = configureStore(initialState);


errorHendlerStoreSet(store);


store.dispatch({
    type: SESSION_RESUMPTION
});


fetcher('register', meets);
fetcher('register', streams);
fetcher('set store', store);
fetcher('start');
fetcher();


class Root extends Component {

    render() {

        return (
            <Provider store={store}>
                <AppRouter />
            </Provider>
        )

    }

}


let rootElement = document.getElementById('root');


ReactDOM.render(
    <Root />, rootElement
);
