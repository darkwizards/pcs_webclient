/**
 * Created by banonotit on 22.12.16.
 */
import Store from "../../../../appComponents/modules/CacheManager";

let s = new Store({get: "getUser"}, Infinity);

/**
 * @typedef {Object} User
 * @property {string} id
 * @property {string} status
 * @property {string} children
 * @property {number} type
 */


export default s;