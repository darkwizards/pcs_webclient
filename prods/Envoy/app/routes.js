import React, {Component} from "react";
import {Route, IndexRoute} from "react-router";
import {ReduxRouter} from "redux-router";
import App from "../../../appComponents/components/App/App";
import Home from "./containers/Home/Home";
import NotFound from "../../../appComponents/components/NotFound/NotFound";
import RegisterTeach from "./containers/RegisterTeach/RegisterTeach";
import Register from "./containers/Register/Register";
import StreamView from "./containers/StreamsView/StreamsView";
import Queue from "./containers/MeetsView/MeetsView";
import MailFailed from "./containers/MailFailed/MailFailed";
import MailReq from "./containers/MailReq/MailReq";
import About from "./containers/About/About";
// import Profile from "./containers/Profile/Profile";
// import LoginPage from "./components/LoginPage/LoginPage";
// import SubscribeView from "./containers/SubscribesView/SubscribesView";
// import MessagesView from "./containers/MessagesView/MessagesView";


export default class AppRouter extends Component {

    render() {
        //Each Route below corresponds to a page
        return (
            <ReduxRouter>
                <Route path="/" component={ App }>
                    <IndexRoute component={ Home }/>
                    <Route path="about" component={ About }/>
                    <Route path="stream" component={ StreamView }/>
                    <Route path="queue" component={ Queue }/>
                    <Route path="register">
                        <IndexRoute component={ Register }/>
                        <Route path="teacher" component={ RegisterTeach }/>
                    </Route>
                    <Route path="mail_failed" component={ MailFailed }/>
                    <Route path="mail_request" component={ MailReq }/>
                    <Route path="*" component={ NotFound }/>
                    {/*<Route path="profile">
                     <IndexRoute component={ Profile }/>
                     <Route path="*" component={ Profile }/>
                     </Route>*/}
                    {/*<Route path="subscribe" component={ SubscribeView }/>*/}
                    {/*<Route path="message" component={ MessagesView }/>*/}
                </Route>
            </ReduxRouter>
        )
    }

}
