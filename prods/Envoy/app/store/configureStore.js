import {createStore, applyMiddleware, compose} from "redux";
import rootReducer from "../reducers";
import {reduxReactRouter} from "redux-router";
import thunkMiddleware from "redux-thunk";
import createHistory from "history/lib/createBrowserHistory";
import Profile from "../middlewares/profile";
import Streams from "../middlewares/streams";
import Meets from "../middlewares/meets";


const createStoreWithMiddleware = compose(
    applyMiddleware(thunkMiddleware, Profile, Streams, Meets),

    reduxReactRouter({
        createHistory
    }),
    // devTools()
)(createStore);


export default function configureStore(initialState) {

    const
        store = createStoreWithMiddleware(rootReducer, initialState);

    if (module.hot && process.env.NODE_ENV != 'production') {

        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers');
            store.replaceReducer(nextRootReducer);
        });

    }

    return store;

}
