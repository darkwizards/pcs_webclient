import React, {Component, PropTypes} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Item from "../../components/MeetItem/MeetItem";
import Login from "../../../../../appComponents/components/RequireLogin/RequireLogin";
import {EDIT_QUEUE} from "../../../../../config/permissions.json";
import {editMeet, deleteMeet, fetchAll, startMeet, endMeet} from "../../../../../appComponents/actions/meets";
import moment from "moment";
import PrintMeetItem from "../../components/PrintMeetItem/PrintMeetItem";
moment.locale("ru");


class Queue extends Component {

    constructor(props) {

        super(props);

        this.state = {
            meets: props.meets.sort((a, b) => a.time_start - b.time_start),
            loaded: false,
            filters: {},
            selected: location.hash.length > 1 ? +location.hash.slice(1) : -1,
            search_min_time: 0,
            state: ''
        }

    }

    componentWillReceiveProps({meets}) {

        document.title = "Встречи [" + this.props.meets.length + "] - VerQ";

        this.setState({
            meets: meets.sort((a, b) => a.time_start - b.time_start)
        })
    }

    componentDidMount() {

        document.title = "Встречи [" + this.props.meets.length + "] - VerQ";

        this.props.fetchAll()

    }

    render() {

        let
            {meets} = this.state,
            mask = this.context.store.getState().session.user.perm;


        let items = meets.slice()
                .map((meet, i, arr) => {

                    let
                        s = [],
                        last = (i != 0) ? arr[i - 1] : {time_start: +new Date(), duration: 0};

                    last = (+last.time_start ) + (+last.duration);

                    if (meet.time_start - last > 3e5)
                        s.push(
                            <h6 className="text-center">
                                {
                                    moment(meet.time_start).from(last)
                                }
                            </h6>
                        );

                    s.push(
                        <Item
                            key={meet.id}
                            queue={meet}
                            expanded={meet.id == this.state.selected}
                            onClick={() => this.setState(state => {

                                if (state.selected == meet.id) {
                                    window.history.pushState(null, null, '#');
                                    return {selected: -1}
                                }

                                window.history.pushState(null, null, '#' + meet.id);
                                return {selected: meet.id}

                            })}
                            onEdit={() => {

                                if (!(mask & EDIT_QUEUE))
                                    return;

                                this.setState({selected: meet.id, state: 'edit'});
                                window.history.pushState(null, null, '#' + meet.id);

                            }}
                            onDel={() => this.props.delete(meet.id)}
                            onStart={() => {
                                this.props.start(meet.id);
                                this.props.fetchAll()
                            }}
                            onEnd={() => this.props.end(meet.id)}
                        />
                    );
                    return s
                }),
            forPrint = meets.slice()
                .map(meet => <PrintMeetItem meet={meet}/>);


        return (
            <Login>
                <div className="col-xs-12">
                    <h2 className="col-xs-12 text-center">Встречи</h2>
                    <p className="col-xs-12 text-center hidden-print">Здесь расположены ближайшие встречи.</p>
                    <p className="hidden-print">
                        Первая встреча начинается <strong className="text-info">
                        {
                            meets[0] ?
                                moment(+meets[0].time_start).format("L LT") :
                                " <Неизвестно>"
                        }
                    </strong>.
                    </p>
                    <div className="form-group">
                        <button className="btn btn-block btn-success btn-lg hidden-print"
                                onClick={() => window.print()}>
                            <i className="glyphicon glyphicon-print"/> Распечатать
                        </button>
                    </div>
                    <div className="col-xs-12 hidden-print">
                        <div className="row">
                            {
                                items.length ?
                                    items :
                                    <h4 className="alert alert-info text-center">
                                        Встреч нет
                                    </h4>
                            }
                        </div>
                    </div>
                    <div className="col-xs-12 visible-print table-responsive">
                        <table className="table table-bordered">
                            {
                                forPrint
                            }
                        </table>
                    </div>
                    {/*</div>*/}
                </div>
            </Login>

        )
    }
}

Queue.contextTypes = {
    store: PropTypes.object
};

// Queue.propsTypes = {};


function mapStateToProps(state) {
    return {
        meets: state.meets,
        router: state.router
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        edit: editMeet,
        "delete": deleteMeet,
        fetchAll,
        start: startMeet,
        end: endMeet
    }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Queue);