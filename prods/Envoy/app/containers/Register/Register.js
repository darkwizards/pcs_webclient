import React, {Component, PropTypes} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {push} from "redux-router";
import {register} from "../../../../../appComponents/actions/profile";
import Checkbox from "../../../../../appComponents/components/Checkbox/Checkbox";
import Select from "../../../../../appComponents/components/react-select";
import Classes from "../../../../../config/select_options/classes.json";
import uniq from "lodash/uniqBy";


class RegisterTeach extends Component {

    constructor(props) {

        super(props);

        this.handleRegistration = this.handleRegistration.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.toggleAccept = this.toggleAccept.bind(this);
        this.handleChildren = this.handleChildren.bind(this);

        this.state = {
            user: {
                name: '',
                mail: '',
                pass: '',
                children: [],
                type: 1,
                status: 'Родитель'
            },
            enabled: false
        };
    }

    /**
     * @function handleRegistration
     * @description Fire onLoginClick function provided to component when login is clicked
     */
    handleRegistration(event) {
        event.preventDefault();
        // this.props.onLoginClick({email: this.state.email, pass: this.state.pass});
        this.props.register({...this.state.user, children: this.state.user.children.map(a => a.value)})
    }

    handleNameChange({target}) {
        this.setState((state) => ({
            user: {
                ...state.user,
                name: target.value
            }
        }))
    };

    /**
     * @function handleUsernameChange
     * @description Update the state with the values from the form inputs.
     * @fires context#setState
     */
    handleUsernameChange({target}) {
        this.setState((state) => ({
            user: {
                ...state.user,
                mail: target.value
            }
        }));
    }


    handlePasswordChange({target}) {
        this.setState((state) => ({
            user: {
                ...state.user,
                pass: target.value
            }
        }));
    }

    handleChildren(children) {
        this.setState(state => ({
            user: {
                ...state.user,
                children: uniq(
                    // what a folly shit is this?
                    children.reduce(
                        (el, all) =>
                            all.value.indexOf("-") ?
                                [
                                    ...el,
                                    {
                                        value: all.value.split("-")[0],
                                        label: all.label.split(" ")[0]
                                    },
                                    all
                                ] :
                                [
                                    ...el,
                                    all
                                ],
                        []
                    ),
                    'value'
                )
            }
        }))
    }

    toggleAccept(a) {
        this.setState({
            enabled: a
        })
    }

    render() {

        if (~this.context.store.getState().session.user.id) {

            this.props.push('/');

            return (
                <div className="col-md-8 col-md-push-2 text-center">
                    <h3 className="alert alert-danger">Вы уже авторизованы</h3>
                </div>
            )

        }

        let valid = this.state.enabled && this.state.user.pass && this.state.user.mail && this.state.user.children.length;

        return (
            <div className="col-md-push-2 col-md-8">
                <h3 className="text-center">Регистрация</h3>
                <div className="login-form">
                    <form onSubmit={this.handleRegistration}>
                        <label className="control-label h6">Ваше ФИО:</label>
                        <div className="form-group">
                            <input
                                onChange={this.handleNameChange}
                                type="text"
                                className="form-control"
                                placeholder="Александров Александр Александрович"
                            />
                            <label
                                htmlFor="login-fios"
                                className="fui-user login-field-icon"
                            />
                        </div>
                        <label className="control-label h6">Группа, в которой Вы учитесь:</label>
                        <div className="form-group">
                            <Select
                                multi
                                onChange={this.handleChildren}
                                placeholder="Группа..."
                                id="login-fios"
                                value={this.state.user.children}
                                options={Classes}
                            />
                        </div>
                        <label className="control-label h6">Ваш Email:</label>
                        <div className="form-group">
                            <input
                                type="email" className="form-control"
                                onChange={this.handleUsernameChange}
                                placeholder="my_mail@example.com" id="login-email"
                            />
                            <label
                                className="login-field-icon fui-mail"
                                htmlFor="login-name"
                            />
                        </div>
                        <label className="control-label h6">Придумайте пароль:</label>
                        <div className="form-group">
                            <input
                                type="password" className="form-control"
                                onChange={this.handlePasswordChange} placeholder="****"
                                id="login-pass"
                            />
                            <label
                                className="login-field-icon fui-lock"
                                htmlFor="login-pass"
                            />
                        </div>
                        <div className="form-group">
                            <Checkbox
                                checked={this.state.enabled}
                                onChange={this.toggleAccept}
                            >
                                <b>Я разрешаю</b> использовать личные данные, введённые мною.
                            </Checkbox>
                        </div>

                        <button
                            className="btn btn-lg btn-block btn-success"
                            disabled={!valid}
                        >
                            Зарегистрироваться
                        </button>

                    </form>
                </div>
            </div>
        )

    }
}


RegisterTeach.contextTypes = {
    store: PropTypes.object
};


function mapDispatchToProps(dispatch) {
    return bindActionCreators({register, push}, dispatch);
}


export default connect((state) => ({router: state.router}), mapDispatchToProps)(RegisterTeach);
