import React, {Component, PropTypes} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Item from "../../components/StreamItem/StreamItem";
import Login from "../../../../../appComponents/components/RequireLogin/RequireLogin";
import Access from "../../../../../appComponents/components/Access/Access";
import {CREATE_STREAM, CREATE_SUBSCRIBE} from "../../../../../config/permissions.json";
import Modal from "react-bootstrap/lib/Modal";
import Create from "../../../../../appComponents/components/StreamForm/StreamForm";
import Classes from "../../../../../config/select_options/classes.json";
import MaskedInput from "react-maskedinput";
import Select from "react-select";
import {add, remove, edit} from "../../../../../appComponents/actions/streams";
import {createMeet, deleteMeet, fetchAll} from "../../../../../appComponents/actions/meets";
import uniq from "lodash/uniq";
import Users from "../../caches/UsersCache";
import Find from "lodash/find";
import moment from "moment";
moment.locale("ru");

class StreamsView extends Component {

    constructor(props) {

        super(props);

        this.state = {
            streams: props.streams,
            loaded: false,
            min_time: Math.max(
                Math.min.apply(null, this.props.streams.map(stream => stream.time_start)),
                +new Date()
            ),
            min_time_set: false,
            selected: location.hash.length > 1 ? +location.hash.slice(1) : -1,
            stat: "",
            search_owner_id: -1,
            owners_id: uniq(props.streams.map(el => '' + el.owner_id)),
            owners: [],
            classes: Classes.filter(a => ~props.usr.children.indexOf(a.value)),
            mounted: false,
            changing_time: false
        };

    }

    componentDidMount() {

        document.title = (this.props.usr.type == 1 ? "Учителя" : "Очереди") + " - VerQ";

        this.props.fetchAll();

        this.state.mounted = true;

        this.getUsers();

    }

    componentWillUnmount() {

        this.state.mounted = false

    }

    /**
     * @param {Array} streams
     */
    componentWillReceiveProps({streams}) {

        this.setState((state) => ({
            streams,
            owners_id: uniq(streams.map(el => '' + el.owner_id)),
            min_time: state.min_time_set ?
                state.min_time :
                Math.max(Math.min.apply(null, streams.map(stream => stream.time_start)), +new Date)
        }));


        this.getUsers();

    }

    getUsers() {

        uniq(this.state.owners_id.map(String)).forEach(el =>
            Users.get(el, this.context.store.getState().session.token)
                .then((data) => {

                    if (this.state.mounted)
                        this.setState((state) => ({
                            owners: uniq(state.owners.concat(data))
                        }))

                })
        );

    }

    render() {

        let
            {user} = this.context.store.getState().session,
            {state} = this,
            items = /** @type {Array.<MyStream>} */state.streams.slice()
                .map(stream =>
                    <Item
                        classes={state.classes}
                        key={stream.id}
                        stream={stream}
                        expanded={state.selected == stream.id}
                        onClick={() => this.setState(state => {

                            if (state.selected == stream.id) {

                                window.history.pushState(null, null, '#');

                                return {selected: -1}

                            } else {

                                window.history.pushState(null, null, '#' + stream.id);

                                return {selected: stream.id}

                            }
                        })}
                        onEdit={() => {

                            window.history.pushState(null, null, '#' + stream.id);

                            this.setState({
                                selected: stream.id,
                                stat: "edit"
                            })

                        }}
                        onSubscribe={() => this.props.meet({
                            stream_id: stream.id,
                            duration: 5,
                            min_time: state.min_time
                        })}
                        onUnsubscribe={() => {

                            let a = Find(this.context.store.getState().meets, {stream_id: stream.id});

                            if (a)
                                this.props.unmeet(a.id);

                        }}
                        onDel={() => {

                            this.props.remove(stream)

                        }}
                    />
                );

        return (
            <Login>
                <div className="col-xs-12">
                    <h2 className="text-center">Очереди</h2>
                    <p className="text-center">
                        Здесь собраны все доступные Вам очереди.<br/>
                        {
                            user.type == 1 ?
                                'Вы можете записаться в любую очередь.' :
                                'Вы можете открыть запись или изменить параметры открытых Вами очередей.'
                        }
                    </p>
                    <div className="row">
                        <Access mask={CREATE_STREAM}>
                            <div
                                className="col-xs-12 col-md-4 col-md-push-4 form-group"
                            >
                                <button
                                    className="btn btn-success right col-xs-12"
                                    onClick={() => this.setState({stat: "create"})}
                                >
                                    Создать очередь
                                </button>
                            </div>
                        </Access>
                        <Access mask={CREATE_SUBSCRIBE}>
                            <div className="col-xs-12">
                                <div>
                                    Если Вы не можете прийти к началу мероприятия, то <a
                                    onClick={() => this.setState({changing_time: true})}
                                    className="btn-link pointer"
                                >
                                    укажите время прихода.
                                </a>
                                    <p>
                                        Сейчас установлено:{' '}
                                        <strong className="text-info">
                                            {moment(this.state.min_time).format("DD\.MM\.YYYY HH:mm")}
                                        </strong>
                                    </p>
                                    <Modal
                                        onHide={() => this.setState({changing_time: false})}
                                        show={state.changing_time}
                                    >
                                        <Modal.Header>
                                            <Modal.Title>
                                                Планируемое время прибытия
                                            </Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            <MaskedInput
                                                mask="R1.O1.\2\011 T1:F1"
                                                value={moment(this.state.min_time).format("DD\.MM\.YYYY HH:mm")}
                                                onChange={({target}) => {
                                                    if (/^(?:[0-2]\d|3[01])\.(?:0[1-9]|1[0-2])\.20\d\d (?:[01]\d|2[0-3]):[0-5]\d$/.test(target.value))
                                                        this.setState({
                                                            min_time: +moment(target.value, "DD\.MM\.YYYY HH:mm"),
                                                            min_time_set: true
                                                        })
                                                }}
                                                className="form-control"
                                                formatCharacters={{
                                                    "O": {
                                                        validate(char){
                                                            return (+char) < 2
                                                        },
                                                        transform(char){
                                                            return char
                                                        }
                                                    },
                                                    "T": {
                                                        validate(char){
                                                            return (+char) < 3
                                                        },
                                                        transform(char){
                                                            return char
                                                        }
                                                    },
                                                    "R": {
                                                        validate(char){
                                                            return (+char) < 4
                                                        },
                                                        transform(char){
                                                            return char
                                                        }
                                                    },
                                                    "F": {
                                                        validate(char){
                                                            return (+char) < 6
                                                        },
                                                        transform(char){
                                                            return char
                                                        }
                                                    }
                                                }}
                                            />
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <button
                                                onClick={() => this.setState({changing_time: false})}
                                                className="btn btn-danger"
                                            >
                                                Закрыть
                                            </button>
                                        </Modal.Footer>
                                    </Modal>
                                </div>
                                <form className="form-group">
                                    <Select
                                        placeholder="Группа"
                                        multi
                                        value={this.state.classes}
                                        onChange={val => this.setState({classes: val})}
                                        options={Classes}
                                    />
                                </form>
                            </div>
                        </Access>
                    </div>
                    <Modal
                        bsSize="large" show={!!this.state.stat}
                        onHide={() => this.setState({stat: null})}
                    >
                        <Modal.Header>
                            <Modal.Title>
                                {state.stat == "edit" ? "Изменение" : "Создание новой"} очереди
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Create
                                defaults={{
                                    owner: user,
                                    ...(state.stat == "edit" ? Find(state.streams, {id: state.selected}) : {})
                                }}
                                onSave={data => {

                                    console.log(data);

                                    if (this.state.stat == "create")
                                        this.props.add({owner_id: user.id, ...data});

                                    else
                                        this.props.edit({...Find(state.streams, {id: state.selected}), ...data});

                                    this.setState({stat: null});
                                }}
                                onCancel={() => this.setState({stat: null})}
                            />
                        </Modal.Body>
                    </Modal>
                    <div>
                        {
                            items.length ?
                                items :
                                <h4 className="alert alert-info text-center">
                                    Очередей нет
                                </h4>
                        }
                    </div>
                </div>
            </Login>
        )
    }

}

StreamsView.contextTypes = {
    store: PropTypes.object
};

StreamsView.propsTypes = {};

function mapStateToProps(state) {
    return {
        usr: state.session.user,
        streams: state.streams,
        router: state.router
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        add,
        remove,
        edit,
        meet: createMeet,
        unmeet: deleteMeet,
        fetchAll
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(StreamsView);
