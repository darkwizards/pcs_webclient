import React, {Component, PropTypes} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {push} from "redux-router";
import {register} from "../../../../../appComponents/actions/profile";
import Checkbox from "../../../../../appComponents/components/Checkbox/Checkbox";
import Select from "../../../../../appComponents/components/react-select";
import Classes from "../../../../../config/select_options/classes.json";
import Specs from "../../../../../config/select_options/specs.json";
import uniq from "lodash/uniqBy";
import {mailDomain as EMAIL} from "../../../../../config/settings.json";
import "../../../../../appComponents/components/react-select/scss/default.scss";


class RegisterTeach extends Component {
    constructor(props) {
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleFioChange = this.handleFioChange.bind(this);
        this.handleStatus = this.handleStatus.bind(this);
        this.handleChildren = this.handleChildren.bind(this);
        this.toggleAccept = this.toggleAccept.bind(this);

        this.state = {
            user: {
                mail: '',
                pass: '',
                status: [],
                children: [],
                type: 2,
                name: '',
            },
            enabled: false
        };
    }

    /**
     * @function handleLogin
     * @description Fire onLoginClick function provided to component when login is clicked
     */
    handleLogin(event) {
        event.preventDefault();
        // this.props.onLoginClick({email: this.state.email, pass: this.state.pass});
        this.props.register({
            ...this.state.user,
            mail: this.state.user.mail + EMAIL,
            children: this.state.user.children.map(a => a.value),
            status: this.state.user.status.map(a => a.value).join(",")
        })
    }

    /**
     * @function handleUsernameChange
     * @description Update the state with the values from the form inputs.
     * @fires context#setState
     */
    handleUsernameChange({target}) {
        this.setState((state) => ({
            user: {
                ...state.user,
                mail: target.value
            }
        }));
    }

    handlePasswordChange({target}) {
        this.setState((state) => ({
            user: {
                ...state.user,
                pass: target.value
            }
        }));
    }

    handleFioChange({target}) {
        this.setState((state) => ({
            user: {
                ...state.user,
                name: target.value
            }
        }));
    }

    toggleAccept(a) {
        this.setState({
            enabled: a
        })
    }

    handleStatus(status) {
        this.setState(state => ({
            user: {
                ...state.user,
                status
            }
        }))
    }

    handleChildren(children) {
        this.setState(state => ({
            user: {
                ...state.user,
                children: uniq(
                    // what a folly shit is this?
                    children.reduce(
                        (all, el) =>
                            el.value.indexOf("-") ?
                                [
                                    ...all,
                                    {
                                        value: el.value.split("-")[0],
                                        label: el.label.split(" ")[0]
                                    },
                                    el
                                ] :
                                [
                                    ...all,
                                    el
                                ],
                        []
                    ),
                    'value'
                )
            }
        }))
    }


    render() {

        if (~this.context.store.getState().session.user.id) {

            this.props.push('/');

            return (
                <div className="col-md-8 col-md-push-2 text-center">
                    <h3 className="alert alert-danger">Вы уже авторизованы</h3>
                    <p>Через 15 секунд Вы вернётесь назад.</p>
                </div>
            )

        }

        let valid = this.state.enabled && this.state.user.pass && this.state.user.mail && this.state.user.name && this.state.user.children.length;

        return (
            <div className="col-md-push-2 col-md-8">
                <h3 className="text-center">Регистрация для сотрудников</h3>
                <div className="login-form ">
                    <form onSubmit={this.handleLogin}>

                        <label className="control-label h6">Ваше ФИО:</label>
                        <div className="form-group">
                            <input
                                type="text" className="form-control"
                                onChange={this.handleFioChange}
                                placeholder="Антон Павлович Чехов"
                                id="login-fios"
                            />
                            <label
                                htmlFor="login-fios"
                                className="fui-user login-field-icon"
                            />
                        </div>

                        <label className="control-label h6">Ваш Email:</label>
                        <div className="form-group">
                            <div className="input-group">
                                <input
                                    value={this.state.user.mail}
                                    type="text" className="form-control"
                                    onChange={this.handleUsernameChange}
                                    placeholder="teacher" id="login-email"
                                />
                                <span
                                    className={"input-group-addon" + (~this.state.user.mail.indexOf("@") ? " input-group-addon-error" : "")}>{EMAIL}</span>
                            </div>
                        </div>

                        <label className="control-label h6">Ваша специализация:</label>
                        <div className="form-group">
                            <Select
                                multi
                                placeholder="Учитель..."
                                options={Specs}
                                onChange={this.handleStatus}
                                value={this.state.user.status}
                            />
                        </div>

                        <label className="control-label h6">Группы в которых Вы ведёте уроки:</label>
                        <div className="form-group">
                            <Select
                                options={Classes}
                                value={this.state.user.children}
                                onChange={this.handleChildren}
                                placeholder="5а, 10б..." multi
                            />
                        </div>


                        <label className="control-label h6">Придумайте пароль:</label>
                        <div className="form-group">
                            <input
                                type="password" className="form-control"
                                onChange={this.handlePasswordChange} placeholder="******"
                                id="login-pass"
                            />
                            <label
                                className="login-field-icon fui-lock"
                                htmlFor="login-pass"
                            />
                        </div>

                        <div className="form-group">
                            <Checkbox
                                checked={this.state.enabled}
                                onChange={this.toggleAccept}
                            >
                                Я разрешаю использовать личные данные, введённые мною.
                            </Checkbox>
                        </div>

                        <button
                            className="btn btn-lg btn-block btn-success"
                            disabled={!valid}
                        >
                            Зарегистрироваться
                        </button>
                    </form>
                </div>
            </div>
        )

    }
}


RegisterTeach.contextTypes = {
    store: PropTypes.object
};


function mapDispatchToProps(dispatch) {
    return bindActionCreators({register, push}, dispatch);
}


export default connect((state) => ({router: state.router}), mapDispatchToProps)(RegisterTeach);
