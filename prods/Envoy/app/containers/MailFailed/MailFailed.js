import React, {Component, PropTypes} from "react";
import {connect} from "react-redux";
import {push} from "redux-router";
import {bindActionCreators} from "redux";


class MailVerified extends Component {

    componentWillReceiveProps() {

        if (this.props.verified)
            this.props.push('/');

    }

    render() {

        return this.props.verified ?
            (
                <div className="col-md-8 col-md-push-2 text-center">
                    <h3 className="alert alert-danger">Вы уже авторизованы</h3>
                </div>
            ) :
            (
                <div className="col-xs-12">
                    <h3 className="col-md-8 col-md-push-2 alert text-center alert-danger">
                        Ссылка не дейстивтельна!
                    </h3>
                    <div className="row">
                        <p className="col-xs-12 text-justify">
                            Возможно Вы используете старую ссылку, возможно Вы скопировали её не полностью. Попробуйте
                            позже.
                        </p>
                        <p className="col-xs-12 text-justify">
                            Если у Вас <b>не получилось</b> подтвердить Ваш email с <b>3 раза</b>, то перешлите
                            письмо-подтверждение в <a href="mailto:verq@vertical1748.ru">службу поддержки</a>{' '}
                            (verq@vertical1748.ru).
                        </p>
                    </div>
                </div>
            )

    }

}

MailVerified.contextTypes = {
    store: PropTypes.object
};


function mapDispatchToProps(dispatch) {
    return bindActionCreators({push}, dispatch);
}

export default connect((state) => ({
    verified: !!~state.session.user.id,
    router: state.router
}), mapDispatchToProps)(MailVerified)