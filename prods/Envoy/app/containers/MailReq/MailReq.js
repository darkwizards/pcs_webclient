import React, {Component, PropTypes} from "react";
import {connect} from "react-redux";
import {push} from "redux-router";
import {bindActionCreators} from "redux";


class MailVerified extends Component {

    componentWillReceiveProps() {

        if (this.props.verified)
            this.props.push('/');

    }

    render() {

        return this.props.verified ?
            (
                <div className="col-md-8 col-md-push-2 text-center">
                    <h3 className="alert alert-danger">Вы уже авторизованы</h3>
                </div>
            ) :
            (
                <div className="col-xs-12">
                    <h3 className="alert alert-success col-md-8 col-md-push-2 text-center">
                        Вам на почту было выслано письмо для подтверждения вашего адреса. После этого будет создана
                        учётная запись.
                    </h3>
                    <div className="row">
                        <p className="col-xs-12 text-justify">
                            <b>Письмо не всегда приходит быстро. Оно может приходить в течение 30-40 минут.</b> Если Вы
                            ждёте его
                            дольше, то, возможно, Вы указали неверный адрес. Пройдите обратно и попробуйте
                            зарегистрироваться
                            заново. Если при повторной регистрации Вам выведется ошибка "такой пользователь существует",
                            то напишите в <a href="mailto:verq@vertical1748.ru">службу поддержки</a>, указав Ваше полное
                            имя и адрес почты, указанный при регистрации.
                        </p>
                    </div>
                </div>
            )

    }

}

MailVerified.contextTypes = {
    store: PropTypes.object
};


function mapDispatchToProps(dispatch) {
    return bindActionCreators({push}, dispatch);
}

export default connect((state) => ({
    verified: state.session.user.name,
    router: state.router
}), mapDispatchToProps)(MailVerified)
