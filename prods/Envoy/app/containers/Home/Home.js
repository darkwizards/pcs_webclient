import {connect} from "react-redux";
import React, {Component} from "react";
import {Link} from "react-router";
import {get} from "axios";
import Login from "../../../../../appComponents/components/RequireLogin/RequireLogin";
import QueueSVG from "../../../../../appComponents/img/queue.svg";
import {apiServer} from "../../../../../config/settings.json";
import {version} from "../../../../../package.json";


class Home extends Component {

    constructor(props) {

        super(props);


    }


    loadAnnounce() {

        if (this.props.session.token && this.props.session.user.children)

            get("/assets/announce" + this.props.session.user.type + ".html")
                .then(({data}) => {

                    this.setState({announce: data})

                })
                .catch(err => {

                    this.setState({announce: '<h4 class="text-danger">Ошибка загрузки</h4>'});

                    console.log(err)

                })

    }

    componentDidMount() {

        document.title = "Главная - VerQ"

    }

    render() {

        return (
            <Login>
                <div>
                    <h1 className="text-center">VerQ</h1>
                    <h6 className="text-center">Система распределения очереди</h6>
                    <div id="queueImg">
                        <img src={QueueSVG} alt="" id="ent"/>
                    </div>
                    {
                        this.props.session.user ?
                            (
                                this.props.session.user.type == 1 ?
                                    (
                                        <div className="text-justify col-xs-12">
                                            <h3 className="text-center">Встретиться с сотрудниками школы</h3>
                                            <p>
                                                Чтобы встретиться с учителем,
                                                Вам следует перейти во
                                                {' '}<Link to="/stream">владку "Учителя"</Link>.
                                                В ней Вы увидите список учителей,
                                                кабинет, где Вы их сможете найти,
                                                и другие комментарии. Нажмите на
                                                кнопку "Записаться", и Вы встанете
                                                в очередь. Если Вы не сможете прийти
                                                к началу мероприятия, выберите удобное Вам время.
                                            </p>
                                            <p>
                                                Полный список учителей, которых
                                                Вы посетите, будет находиться во
                                                {' '}<Link to="/queue">вкладке "Встречи"</Link>.
                                                Если Вы передумали идти к тому
                                                или иному преподавателю, нажмите
                                                на кнопку "Отменить" напротив его имени.
                                            </p>
                                        </div>
                                    ) :
                                    (
                                        <div className="text-justify col-xs-12">
                                            <h3 className="text-center">Открыть запись на приём</h3>
                                            <p>
                                                Чтобы родители смогли встретиться с
                                                Вами, Вам следует открыть запись на
                                                прием. Для этого пройдите во
                                                {' '}<Link to="/stream">владку "Очереди"</Link>{' '}и
                                                нажмите там на кнопку "Открыть запись".
                                                Заполните форму ее создания и сохраните.
                                                При необходимости Вы сможете ее редактировать.
                                            </p>
                                            <p>
                                                Когда родитель решит прийти к Вам,
                                                Вы получите уведомление об этом.
                                                Полный список родителей, желающих
                                                с Вами поговорить, будет во вкладке
                                                {' '}<Link to="/queue">"Встречи"</Link>.
                                            </p>
                                        </div>
                                    )
                            ) :
                            (
                                <div className="text-center col-xs-12">
                                    <h1>Загрузка...</h1>
                                </div>
                            )
                    }
                </div>
            </Login>
        )

    }

}


export default connect(state => ({
    session: state.session
}), () => ({}))(Home)
