import React, {Component} from "react";
import {
    Button,
    HelpBlock,
    Radio,
    FormGroup,
    ControlLabel,
    Jumbotron,
    ButtonToolbar,
    FormControl,
    Row,
    Col,
    Alert
} from "react-bootstrap";
import Checkbox from "../../../../../appComponents/components/Checkbox/Checkbox";

export default class About extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="col-xs-12">
                <h1>Header</h1>
                <h2>Header</h2>
                <h3>Header</h3>
                <h4>Header</h4>
                <h5>Header</h5>
                <h6>Header</h6>
                <p>
                    <ButtonToolbar>
                        {/* Standard button */}
                        <Button>Default</Button>

                        {/* Provides extra visual weight and identifies the primary action in a set of buttons */}
                        <Button bsStyle="primary">Primary</Button>

                        {/* Indicates a successful or positive action */}
                        <Button bsStyle="success">Success</Button>

                        {/* Contextual button for informational alert messages */}
                        <Button bsStyle="info">Info</Button>

                        {/* Indicates caution should be taken with this action */}
                        <Button bsStyle="warning">Warning</Button>

                        {/* Indicates a dangerous or potentially negative action */}
                        <Button bsStyle="danger">Danger</Button>

                        {/* Deemphasize a button by making it look like a link while maintaining button behavior */}
                        <Button bsStyle="link">Link</Button>
                    </ButtonToolbar>
                </p>
                <Jumbotron>Тестовый инетрфейс</Jumbotron>
                <form>
                    <FieldGroup
                        id="formControlsText"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        autoFocus={true}
                    />
                    <FieldGroup
                        id="formControlsEmail"
                        type="email"
                        label="Email address"
                        placeholder="Enter email"
                    />
                    <FieldGroup
                        id="formControlsPassword"
                        label="Password"
                        type="password"
                        disabled
                    />
                    <FieldGroup
                        id="formControlsFile"
                        type="file"
                        label="File"
                        help="Example block-level help text here."
                    />

                    <Checkbox checked readOnly>
                        Checkbox
                    </Checkbox>
                    <Radio checked readOnly>
                        Radio
                    </Radio>

                    <FormGroup>
                        <Checkbox inline>
                            1
                        </Checkbox>
                        {' '}
                        <Checkbox inline>
                            2
                        </Checkbox>
                        {' '}
                        <Checkbox inline>
                            3
                        </Checkbox>
                    </FormGroup>
                    <FormGroup>
                        <Radio inline>
                            1
                        </Radio>
                        {' '}
                        <Radio inline>
                            2
                        </Radio>
                        {' '}
                        <Radio inline>
                            3
                        </Radio>
                    </FormGroup>

                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>Select</ControlLabel>
                        <FormControl componentClass="select" placeholder="select">
                            <option value="select">select</option>
                            <option value="other">...</option>
                        </FormControl>
                    </FormGroup>
                    <FormGroup controlId="formControlsSelectMultiple">
                        <ControlLabel>Multiple select</ControlLabel>
                        <FormControl componentClass="select" multiple>
                            <option value="select">select (multiple)</option>
                            <option value="other">...</option>
                        </FormControl>
                    </FormGroup>

                    <FormGroup controlId="formControlsTextarea">
                        <ControlLabel>Textarea</ControlLabel>
                        <FormControl componentClass="textarea" placeholder="textarea"/>
                    </FormGroup>

                    <FormGroup>
                        <ControlLabel>Static text</ControlLabel>
                        <FormControl.Static>
                            email@example.com
                        </FormControl.Static>
                    </FormGroup>

                    <Button type="submit">
                        Submit
                    </Button>
                </form>
                <p>

                    <Row>
                        <Col xs={12}>
                            <Alert bsStyle="danger">Danger</Alert>
                        </Col>
                        <Col xs={12}>
                            <Alert bsStyle="info">info</Alert>
                        </Col>
                        <Col xs={12}>
                            <Alert bsStyle="warning">warning</Alert>
                        </Col>
                        <Col xs={12}>
                            <Alert bsStyle="success">success</Alert>
                        </Col>
                    </Row>
                </p>
            </div>
        )
    }
}
function FieldGroup({id, label, help, ...props}) {
    return (
        <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl {...props} />
            {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
    );
}
