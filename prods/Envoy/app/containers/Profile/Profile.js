import React, {Component, PropTypes} from "react";
import RequireLogin from "../../../../../appComponents/components/RequireLogin/RequireLogin";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {push} from "redux-router";

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            getting: true,
            user: null,
            subscribes: [],
            streams: [],
            error: false
        };
    }

    updateUData(user) {

    }


    componentWillReceiveProps({user}) {
        this.updateUData(user);
    }

    componentDidMount() {
        this.updateUData(this.props.user);
    }

    render() {
        let {user, getting, error}=this.state,
            streams = this.state.streams.slice()
                .map(stream =>
                    <StreamItem
                        prev
                        key={stream.id}
                        stream={stream}
                        onClick={() => this.props.push('/stream#' + stream.id)}
                    />
                );

        return (
            <RequireLogin>
                {
                    getting ?
                        <p className="text-info text-center text-capitalize">Загрузка...</p> :
                        error || !user || !("id" in user) ?
                            <h3 className="text-center col-md-push-2 col-md-8 alert alert-danger">Ошибка загрузки</h3> :
                            <div>
                                <h1 className="text-center">
                                    {user.children} <sup className="badge">{user.status_name}</sup>
                                </h1>
                                <div className="col-xs-12 col-md-6">
                                    <h4 className="text-center">Очереди пользователя</h4>
                                    {
                                        streams.length ?
                                            streams :
                                            <h5 className="text-center alert alert-info">Нету потоков</h5>
                                    }

                                </div>
                            </div>


                }
            </RequireLogin>
        )
    }
}

Profile.contextTypes = {
    store: PropTypes.object
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({push}, dispatch);
}
export default connect((state) => ({
    user: state.router.params.splat || state.session.user.id
}), mapDispatchToProps)(Profile)