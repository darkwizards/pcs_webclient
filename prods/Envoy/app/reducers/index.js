import {combineReducers} from "redux";
import {routerStateReducer} from "redux-router";
import session from "../../../../appComponents/reducers/profile";
import notice from "../../../../appComponents/reducers/notify";
import streams from "../../../../appComponents/reducers/streams";
import meets from "../../../../appComponents/reducers/meets";
// import currentEmployee from "./current_condition";


const rootReducer = combineReducers({
    // currentEmployee,
    meets,
    session,
    notice,
    streams,
    router: routerStateReducer
});


export default rootReducer;
