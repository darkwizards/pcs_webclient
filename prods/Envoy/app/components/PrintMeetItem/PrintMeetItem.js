import React, {Component, PropTypes} from "react";
import Users from "../../caches/UsersCache";
import Streams from "../../caches/StreamsCache";
import moment from "moment";
moment.locale('ru');

export default class PrintMeetItem extends Component {

    constructor(props) {

        super(props);
        this.state = {
            teacher: null,
            stream: null
        }

    }

    componentDidMount() {

        let token = this.context.store.getState().session.token;

        Users.get(this.props.meet.recipient_id, token)
            .then(teacher => this.setState({teacher}))
            .catch(err => {
                console.log(err);
                alert("Не удалось получить данные об учителе")
            });

        Streams.get(this.props.meet.stream_id, token)
            .then(stream => this.setState({stream}))
            .catch(err => {
                console.log(err);
                alert("Не удалось получить данные об очереди")
            })
    }

    render() {
        return (
            <tr>
                <td>
                    <b className="h6">
                        {
                            this.state.teacher ?
                                <span>{this.state.teacher.name} [{this.state.teacher.status}]</span> :
                                "Загрузка..."
                        }
                    </b>
                    <div>
                        {
                            this.state.stream ?
                                this.state.stream.description :
                                "Загрузка..."
                        }
                    </div>
                </td>
                <td className="text-center">
                    <div className="h6">
                        {
                            moment(new Date(+this.props.meet.time_start)).format("DD.MM LT")
                        }
                    </div>
                </td>
            </tr>
        )
    }
}

PrintMeetItem.contextTypes = {
    store: PropTypes.object
};

PrintMeetItem.propsTypes = {};