import React, {Component, PropTypes} from "react";
import "../../../../../appComponents/styles/ListItem.scss";
import Collapse from "react-bootstrap/lib/Collapse";
import Spec from "../../../../../config/select_options/specs.json";
import moment from "moment";
import Users from "../../caches/UsersCache";
import {EDIT_STREAM, DELETE_STREAM, CREATE_SUBSCRIBE} from "../../../../../config/permissions.json";

moment.locale("ru");


export default class StreamItem extends Component {

    constructor(props) {

        super(props);

        this.state = {
            ex: props.expanded,
            subscribed: false,
            mounted: false
        }

    }

    componentWillReceiveProps({expanded:ex}) {

        this.setState({
            ex,
            subscribed: this.context.store.getState().meets.filter(el => el.stream_id == this.props.stream.id)[0]
        })

    }

    componentDidMount() {

        this.state.mounted = true;

        let state = this.context.store.getState();

        this.setState({
            subscribed: this.context.store.getState().meets.filter(el => el.stream_id == this.props.stream.id)[0]
        });

        Users
            .get(this.props.stream.owner_id, state.session.token)
            .then(data => {

                if (this.state.mounted)
                    this.setState({owner: data});

            })
            .catch(err => console.dir(err));

    }

    componentWillUnmount() {

        this.state.mounted = false;

    }

    render() {

        let
            {stream, classes} = this.props,
            {owner} = this.state,
            {user} = this.context.store.getState().session,
            isOwner = owner && owner.id === user.id,

            btns = [{
                allow: CREATE_SUBSCRIBE & user.perm,
                el: (
                    <button
                        key="s" className={"btn btn-" + (this.state.subscribed ? "danger" : "success")}

                        onClick={() => this.state.subscribed ? this.props.onUnsubscribe() : this.props.onSubscribe()}
                        title="Записаться..."
                    >
                        {
                            this.state.subscribed ?
                                <span>
                                    <span className="visible-sm visible-md visible-lg">Отменить встречу</span>
                                    <span className="hidden-sm hidden-md hidden-lg glyphicon glyphicon-log-out"/>
                                </span> :
                                <span>
                                    <span className="visible-sm visible-md visible-lg">Встретиться</span>
                                    <span className="hidden-sm hidden-md hidden-lg glyphicon glyphicon-log-in"/>
                                </span>
                        }
                    </button>
                )
            }, {
                allow: EDIT_STREAM & user.perm,
                el: (
                    <button
                        title="Редактировать..."
                        key="c" className="btn btn-warning"
                        onClick={() => this.props.onEdit()}
                    >
                        <span className="visible-sm visible-md visible-lg">Редактировать</span>
                        <span className="hidden-sm hidden-md hidden-lg glyphicon glyphicon-edit"/>
                    </button>
                )
            }, {
                allow: DELETE_STREAM & user.perm,
                el: (
                    <button
                        title="Удалить..."
                        key="d" className="btn btn-danger" onClick={() => this.props.onDel()}
                    >
                        <span className="visible-sm visible-md visible-lg">Удалить</span>
                        <span className="hidden-sm hidden-md hidden-lg glyphicon glyphicon-trash"/>
                    </button>
                )
            }, {
                allow: true,
                el: (
                    <button
                        key="ex" title="Развернуть/свернуть..."
                        className="btn btn-default btn-primary" onClick={() => this.props.onClick()}
                    >
                        {
                            this.props.expanded ?
                                <span className="glyphicon glyphicon-chevron-up"/> :
                                <span className="glyphicon glyphicon-chevron-down"/>
                        }
                    </button>
                )
            }].filter(el => el.allow).map(({el}) => el);


        if (classes.length && owner && !classes.some(a => ~owner.children.indexOf(a.value)))
            return null;


        return (
            <div className="list-item login-form">
                {
                    this.props.prev ?
                        '' :
                        <div className="btn-group right">
                            {btns}
                        </div>
                }
                <h6
                    className="title left pointer"
                    onClick={() => {
                        this.props.onClick()
                    }}
                >
                    {
                        isOwner ?
                            "Вы" :
                            [
                                owner ?
                                    owner.name + ' ' :
                                    "Загрузка... ",
                                (
                                    <span className="badge">
                                    {
                                        owner ?
                                            Spec.reduce((prev, cur, i, arr) => {
                                                if (cur.value == owner.status)
                                                    return cur.label;
                                                else if (prev === null && i == arr.length - 1)
                                                    return owner.status;
                                                else
                                                    return prev
                                            }, null) || "no_status" :
                                            "Загрузка..."
                                    }
                                    </span>
                                )
                            ]
                    }{' '}
                    <span className={"hidden-xs hidden-sm " + (!!this.state.subscribed ? "text-info" : "")}>
                        [{
                            this.state.subscribed ?
                                moment(+this.state.subscribed.time_start).format("DD.MM LT") :
                                moment(+stream.time_start).format("DD.MM LT") + '-' + moment(+stream.time_end).format("LT")
                        }]
                    </span>
                </h6>
                <Collapse in={this.props.expanded} onClick={ev => ev.stopPropagation()}>
                    <div>
                        <span className="hidden-md hidden-lg">
                            Пройдёт {moment(+stream.time_start).format("DD.MM LT")}{' '}
                            до {moment(+stream.time_end).format("LT")}
                        </span>
                        <div>{stream.description}</div>
                        {
                            owner ? (
                                    <div key="spec">
                                        {/*<b>Специализация учителя</b>: {owner.status}*/}
                                    </div>
                                ) :
                                null
                        }
                    </div>
                </Collapse>
            </div>
        )
    }
}


StreamItem.contextTypes = {
    store: PropTypes.object
};

StreamItem.defaultProps = {
    classes: [],
    expanded: false,
    onDel(){
    },
    onEdit(){
    },
    onSubscribe(){
    },
    onUnsubscribe(){
    },
    prev: false
};

StreamItem.propsTypes = {
    stream: PropTypes.object.isRequired,
    classes: PropTypes.array.isRequired,
    onEdit: PropTypes.func,
    onSubscribe: PropTypes.func,
    onUnsubscribe: PropTypes.func,
    onDel: PropTypes.func,
    onClick: PropTypes.func.isRequired,
    expanded: PropTypes.bool
};
