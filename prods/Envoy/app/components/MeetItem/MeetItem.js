import React, {Component, PropTypes} from "react";
import Collapse from "react-bootstrap/lib/Collapse";
import {EDIT_QUEUE, DELETE_SUBSCRIBE} from "../../../../../config/permissions.json";
import {Link} from "react-router";
import "./MeetItem.scss";
import count, {ready} from "../../../../../appComponents/modules/RusNum";
import Without from "lodash/without";
import Streams from "../../caches/StreamsCache";
import Users from "../../caches/UsersCache";
import moment from "moment";
moment.locale("ru");

export default class MeetItem extends Component {
    constructor(props) {

        super(props);

        this.state = {
            loaded: false,
            title: null,
            owner: null,
            recipient: null,
            description: null,
            process: 0,
            mounted: false
        };

        this.tick = this.tick.bind(this);

    }


    tick() {

        // don't spawn errors if unmounted
        if (!this.state.mounted)
            return;

        // payload
        this.setState({
            process: this.props.queue.started !== 0 && this.props.queue.ended === 0 ?
                Math.min(
                    1,
                    (+new Date() - this.props.queue.started) / (this.props.queue.duration * 6e4)
                ) :
                0
        });

        // next step

        setTimeout(this.tick, 1e3)

    }

    componentWillReceiveProps({queue}) {

        if (queue.started !== 0 && queue.ended === 0)
            this.tick();

    }

    componentDidMount() {

        let queue = this.props.queue;

        this.state.mounted = true;

        if (queue.started !== 0 && queue.ended === 0)
            this.tick();

        let
            store = this.context.store,
            ses = store.getState().session,

            needName_id = Without([queue.owner_id, queue.recipient_id], ses.user.id),

            users = needName_id.map(needName => Users.get(needName, ses.token)),

            stream = Streams.get(queue.stream_id, ses.token)
            ;


        Promise.all(users)
            .then((data) => {

                if (this.state.mounted)
                    this.setState({
                        owner: data.filter(usr => usr.id === queue.owner_id)[0],
                        recipient: data.filter(usr => usr.id === queue.recipient_id)[0]
                    })

            })
            .catch(
                err => console.dir(err)
            );


        stream
            .then(data => {

                if (this.state.mounted)
                    this.setState({description: data.description})

            })
            .catch(err => console.dir(err));

    }

    componentWillUnmount() {

        this.state.mounted = false;

    }

    render() {

        let
            {user} = this.context.store.getState().session,
            que = this.props.queue,
            buttons = [{
                allow: !que.ended && que.started !== 0 && que.ended === 0 && !!(user.perm & EDIT_QUEUE),
                el: (
                    <button
                        key="a" className="btn btn-warning"
                        onClick={() => this.props.onEnd()}
                    >
                        <span className="visible-sm visible-md visible-lg">Закончить</span>
                        <span className="hidden-sm hidden-md hidden-lg glyphicon glyphicon-stop"/>
                    </button>
                )
            }, {
                allow: !que.ended && que.started === 0 && !!(user.perm & EDIT_QUEUE),
                el: (
                    <button
                        key="r" className="btn btn-success"
                        onClick={() => this.props.onStart()}
                    >
                        <span className="visible-sm visible-md visible-lg">Начать</span>
                        <span className="hidden-sm hidden-md hidden-lg glyphicon glyphicon-play"/>
                    </button>
                )
            }, {
                allow: DELETE_SUBSCRIBE & user.perm,
                el: (
                    <button
                        key="d" className="btn btn-danger"
                        onClick={() => this.props.onDel()}
                    >
                        <span className="visible-sm visible-md visible-lg">Отменить</span>
                        <span className="hidden-sm hidden-md hidden-lg glyphicon glyphicon-ban-circle"/>
                    </button>
                )
            }, {
                allow: true,
                el: (
                    <button
                        key='e'
                        className="btn btn-default btn-primary"
                        title="Рскрыть"
                        onClick={() => this.props.onClick()}
                    >
                        {
                            this.props.expanded ?
                                <span className="glyphicon glyphicon-chevron-up"/> :
                                <span className="glyphicon glyphicon-chevron-down"/>
                        }
                    </button>
                )
            }].filter(el => el.allow).map(a => a.el);

        return (
            <div
                className="list-item login-form que"
            >
                <div
                    className="embed"
                >
                    <div
                        className="btn-group"
                    >
                        {buttons}
                    </div>
                    <h6
                        className="title"
                    >
                        Встреча с {
                        this.state.owner || this.state.recipient ?
                            <Link to={"/stream#" + que.stream_id}>{
                                [
                                    this.state.owner && this.state.owner.name,
                                    this.state.recipient && this.state.recipient.name
                                ].filter(e => !!e).join(" -> ")
                            }</Link> :
                            <span className="text-info">Загрузка данных...</span>
                    } пройдёт {moment(+que.time_start).format("DD MMM kk:mm")}
                    </h6>
                    <Collapse in={this.props.expanded}>
                        <div>
                            <p>Дополнительная информация: {this.state.description ||
                            <span className="text-info">Загрузка данных...</span>}</p>
                            <p>Длительность встречи: ~{que.duration} {count(ready.time.mins, que.duration)}</p>
                            {/*{
                                this.state.recipient ? (
                                        <div>
                                            <p>Информация об учителе:</p>
                                            <ui>
                                                <li>Предмет: {this.state.recipient.status}</li>
                                                <li>Классы: {this.state.recipient.children.join(", ")}</li>
                                            </ui>
                                        </div>
                                    ) :
                                    ''
                             }*/}
                            {/*{
                                this.state.owner ? (
                                        <div>
                                            <p>Дополнительная информация о родителе:</p>
                                            <ui>
                                                <li>Дети: {this.state.owner.children.join(", ")}</li>
                                            </ui>
                                        </div>
                                    ) :
                                    ''
                             }*/}
                        </div>
                    </Collapse>
                    <div
                        style={
                            {width: this.state.process * 100 + '%'}
                        }
                        className="progress-bar"
                    />
                </div>
            </div>
        )
    }
}
MeetItem.defaultProps = {
    onClick: function () {
    },
    onEdit(){
    },
    onDel(){
    },
    onStart(){
    },
    onEnd(){
    }
};

MeetItem.contextTypes = {
    store: PropTypes.object
};

MeetItem.propsTypes = {
    queue: PropTypes.object,
    expanded: PropTypes.bool
};