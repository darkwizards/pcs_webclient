import React, {Component, PropTypes} from "react";
import Datetime from "../../../../../appComponents/components/react-datetime";


class QueueForm extends Component {

    constructor(props) {
        console.log(props);

        super(props);

        this.state = {
            time_start: +new Date(props.defaults.time_start) || +(new Date()),
            title: "",
            ...props.defaults,
            defaults: props.defaults,
        };

    }

    save() {

        console.log(new Date(this.state.time_start));

        if (this.state.time_start != this.state.defaults.time_start)
            this.props.onSave({
                ...this.state.defaults,
                time_start: this.state.time_start
            });

        else
            this.props.onCancel()

    }


    render() {
        return (
            <form
                onSubmit={ev => {
                    ev.preventDefault();
                    this.save();
                }}
            >
                <div
                    className="form-group"
                >
                    {this.state.title}
                </div>
                <Datetime
                    onChange={a => this.setState({time_start: +a})}
                    className="form-group"
                    value={new Date(this.state.time_start)}
                    inputProps={{
                        placeholder: "Введите дату и время",
                        className: "form-control"
                    }}
                />
                <div
                    className="form-group btn-group"
                >
                    <input type="submit" className="btn btn-success" value="Сохранить"/>
                    <button
                        onClick={(e) => {
                            e.preventDefault();
                            this.props.onCancel();
                        }}
                        className="btn btn-info"
                    >
                        Отменить
                    </button>
                </div>
            </form>
        )
    }
}

QueueForm.contextTypes = {
    store: PropTypes.object
};


QueueForm.propsTypes = {
    defaults: PropTypes.object,
    onSave: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired
};


export default QueueForm;