export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_RESPONSE = 'LOGIN_RESPONSE';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGOUT = 'LOGOUT';
export const USER_DATA_RECIVE = 'USR_DAT_RECIVE';
export const SESSION_RESUMPTION = 'SESS_RESUMP';


import axios from "axios";
import {apiServer as apiUrl} from "../../../../config/settings.json";
import md5 from "js-md5";
import qs from "querystring";
import {notify} from "../../../../appComponents/actions/notice";
import Handel from "../../../../appComponents/modules/ErrorHandler";


export function login({mail, pass}) {
    // alert(arguments);

    pass = md5(pass);

    return (dispatch, State) => {

        let sess = State().session;

        if (~sess.user.id)
            return;

        dispatch({
            type: LOGIN_REQUEST
        });

        // alert("Login...");
        axios
            .post(apiUrl + '/login', qs.stringify({mail, pass}))
            .then(function ({data}) {

                dispatch({
                    type: LOGIN_RESPONSE,
                    data
                });

            })
            .catch(function (error) {

                dispatch({
                    type: LOGIN_ERROR,
                    error
                });

                dispatch(notify({
                    text: "Ошибка входа в систему",
                    type: 3
                }));

                return Handel(error, dispatch)

            })
    }
}

export function logout() {
    return {
        type: LOGOUT
    }
}
