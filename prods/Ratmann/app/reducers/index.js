import {combineReducers} from "redux";
import {routerStateReducer} from "redux-router";
import session from "../../../../appComponents/reducers/profile";
import notice from "../../../../appComponents/reducers/notify";
import users from "./users";
import streams from "../../../../appComponents/reducers/streams";


const rootReducer = combineReducers({
    session,
    streams,
    notice,
    users,
    router: routerStateReducer
});


export default rootReducer;
