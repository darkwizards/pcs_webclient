import React, {Component} from "react";
import {Route, IndexRoute} from "react-router";
import {ReduxRouter} from "redux-router";
import App from "../../../appComponents/components/App/App";
import Home from "./containers/Home/Home";
import Users from "./containers/Users/Users";
import NotFound from "../../../appComponents/components/NotFound/NotFound";


export default class AppRouter extends Component {

    render() {
        //Each Route below corresponds to a page
        return (
            <ReduxRouter>
                <Route path="/admin" component={ App }>
                    <IndexRoute component={ Home }/>
                    <Route path="users" component={ Users }/>
                    <Route path="*" component={ NotFound }/>
                </Route>
            </ReduxRouter>
        )
    }

}
