import React, {Component, PropTypes} from "react";
import Equal from "lodash/isEqual";
import "react-select/scss/default.scss";
import Select from "react-select";
import Specs from "../../../../../config/select_options/specs.json";
import Classes from "../../../../../config/select_options/classes.json";
import Groups from "../../../../../config/select_options/groups.json";
import uniq from "lodash/uniqBy";


class UserForm extends Component {

    constructor(props) {

        super(props);

        this.state = {
            name: "",
            ...props.defaults,
            status: Specs.slice().filter(a => a.value == props.defaults.status)[0] || {
                value: props.defaults.status,
                label: props.defaults.status
            },
            pass: Object.keys(props.defaults).length ? "=!=" : "",
            type: Groups.slice().filter(a => a.value == props.defaults.type)[0] || Groups[0],
            children: Classes.slice().filter(a => ~(props.defaults.children || []).indexOf(a.value)),
        };

        this.handleChildren = this.handleChildren.bind(this);

    }

    save() {

        let
            state = this.state,
            props = this.props.defaults,
            fromDef = {
                id: props.id,
                name: props.name,
                type: props.type,
                status: props.status,
                children: props.children,
                pass: "=!=",
                mail: props.mail
            },
            res = {
                id: props.id,
                name: state.name,
                type: state.type.value,
                status: state.status.value,
                children: state.children.map(a => a.value),
                pass: state.pass,
                mail: state.mail
            };


        if (!Equal(res, fromDef))
            this.props.onSave(res);
        else
            this.props.onCancel()

    }


    handleChildren(children) {
        this.setState(state => ({
            children: uniq(
                // what a folly shit is this?
                children.reduce(
                    (all, el) =>
                        el.value.indexOf("-") ?
                            [
                                ...all,
                                {
                                    value: el.value.split("-")[0],
                                    label: el.label.split(" ")[0]
                                },
                                el
                            ] :
                            [
                                ...all,
                                el
                            ],
                    []
                ),
                'value'
            )
        }))
    }

    render() {
        return (
            <form
                onSubmit={ev => {
                    ev.preventDefault();
                    this.save();
                }}
            >
                <div
                    className="form-group"
                >
                    <label className="control-label h6">ФИО пользователя</label>
                    <input
                        type="text"
                        className="form-control"
                        value={this.state.name}
                        onChange={ev => this.setState({name: ev.target.value})}
                    />
                </div>
                <div className="form-group">
                    <label className="control-label h6">Mail адрес пользователя</label>
                    <input
                        required="required"
                        className="form-control"
                        type="email"
                        value={this.state.mail}
                        onChange={ev => this.setState({mail: ev.target.value.toLowerCase()})}
                    />
                </div>
                <div className="form-group">
                    <label className="control-label h6">Тип в системе</label>
                    <Select
                        required
                        options={Groups}
                        onChange={type => {
                            this.setState({
                                children: [],
                                status: {},
                                type
                            })
                        }}
                        value={this.state.type}
                    />
                </div>
                <div
                    className={"form-group " + (this.state.type.value === 1 ? 'hidden' : '')}
                >
                    <label className="control-label h6">Специализация сотрудника</label>
                    <Select
                        required={this.state.type == 2}
                        placeholder="Русский Язык"
                        options={Specs}
                        onChange={(status) => this.setState({status})}
                        value={this.state.status}
                    />
                </div>
                <div
                    className="form-group"
                >
                    <label className="control-label h6">Классы, в которых {
                        this.state.type == 1 ?
                            "учатся дети" :
                            "ведёт учитель"
                    }
                    </label>
                    <Select
                        required
                        multi
                        placeholder="Учитель..."
                        options={Classes}
                        onChange={this.handleChildren}
                        value={this.state.children}
                    />
                </div>
                <div
                    className="form-group"
                >
                    <label
                        className="control-label h6"
                    >
                        Пароль пользователя
                    </label>
                    <p
                        style={{
                            display: Object.keys(this.props.defaults).length != 0 ? "" : "none"
                        }}
                    >
                        Если хотите оставить текущий пароль, то вместо пароля вставьте <b>===</b>
                    </p>
                    <input
                        required={Object.keys(this.props.defaults).length == 0}
                        className="form-control"
                        onChange={ev => {
                            this.setState({pass: ev.target.value})
                        }}
                        value={this.state.pass}
                    />
                </div>
                <div
                    className="form-group btn-group"
                >
                    <input type="submit" className="btn btn-success" value="Создать"/>
                    <button
                        onClick={(e) => {
                            e.preventDefault();
                            this.props.onCancel();
                        }}
                        className="btn btn-danger"
                    >
                        Отменить
                    </button>
                </div>
            </form>
        )
    }
}

UserForm.contextTypes = {
    store: PropTypes.object
};


UserForm.propsTypes = {
    defaults: PropTypes.object,
    onSave: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired
};
export default UserForm;