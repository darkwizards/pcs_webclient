import React, {Component, PropTypes} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Specs from "../../../../../config/select_options/specs.json";
import "./UserItem.scss";

// import Classes from "../../../../../config/select_options/classes.json";

class UserItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editing: '',
            name: '',
            mail: '',
            type: 2,
            status: '',

        }
    }

    render() {


        let {user: usr}=this.props;

        return (
            <tr className="verq-user-item">
                <td>{usr.name}</td>
                <td>{usr.mail}</td>
                <td>{(["Администратор", "Родитель", "Учитель"][usr.type])}</td>
                <td>
                    {
                        (Specs.slice().filter(a => a.value == usr.status)[0] || {}).label || usr.status // shitty code
                    }

                    <div className="btn-group">
                        {
                            this.props.prof && usr.id === this.props.prof.id ?
                                null :
                                <button
                                    onClick={() => this.props.onDel()}
                                    className="btn btn-danger"
                                >
                                    <span className="glyphicon glyphicon-trash"/>
                                </button>
                        }
                        <button
                            onClick={() => this.props.onEdit()}
                            className="btn btn-warning"
                        >
                            <span className="glyphicon glyphicon-pencil"/>
                        </button>
                    </div>
                </td>
            </tr>
        )
    }
}


UserItem.contextTypes = {
    store: PropTypes.object
};


UserItem.propsTypes = {
    user: PropTypes.object,
    onDel: PropTypes.func,
    onEdit: PropTypes.func
};


function mapStateToProps(state) {
    return {
        prof: state.session.user,
        router: state.router
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UserItem);
