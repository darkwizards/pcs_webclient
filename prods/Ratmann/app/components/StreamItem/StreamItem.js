import React, {Component, PropTypes} from "react";
import "../../../../../appComponents/styles/ListItem.scss";
import moment from "moment";
import Users from "../../caches/UsersCache";

moment.locale("ru");


export default class StreamItem extends Component {

    constructor(props) {

        super(props);

        this.state = {
            mounted: false
        }

    }

    componentDidMount() {

        this.state.mounted = true;

        let state = this.context.store.getState();

        Users
            .get(this.props.stream.owner_id, state.session.token)
            .then(data => {

                if (this.state.mounted)
                    this.setState({owner: data});

            })
            .catch(err => console.dir(err));

    }

    componentWillUnmount() {

        this.state.mounted = false;

    }

    render() {

        let
            {stream} = this.props,
            {owner} = this.state,
            {user} = this.context.store.getState().session,
            isOwner = owner && owner.id === user.id;

        if (owner && (this.props.filtred.length == 0 || this.props.filtred.some(el => ~owner.children.indexOf(el.value))))

            return (
                <div className="list-item login-form">
                    <div className="btn-group right">
                        <button
                            key="s" className={"btn btn-" + (this.props.subscribed ? "danger" : "success")}

                            onClick={() => this.props.subscribed ? this.props.onUnsubscribe() : this.props.onSubscribe()}
                            title="Записаться..."
                        >
                            {
                                this.props.subscribed ?
                                    <span>
                                    <span className="visible-sm visible-md visible-lg">Отменить встречу</span>
                                    <span className="hidden-sm hidden-md hidden-lg glyphicon glyphicon-log-out"/>
                                </span> :
                                    <span>
                                    <span className="visible-sm visible-md visible-lg">Встретиться</span>
                                    <span className="hidden-sm hidden-md hidden-lg glyphicon glyphicon-log-in"/>
                                </span>
                            }
                        </button>
                    </div>

                    <h6
                        className="title left pointer"
                        onClick={() => {
                            this.props.onClick()
                        }}
                    >
                        {
                            isOwner ?
                                "Вы" :
                                [
                                    owner ?
                                        owner.name :
                                        "Загрузка...",
                                    (<span className="badge">
                                    {
                                        owner ?
                                            owner.status || "no_status" :
                                            "Загрузка..."
                                    }
                                </span>)
                                ]
                        } [{moment(+stream.time_start).format("L LT")}]
                    </h6>
                </div>
            );
        else
            return null
    }
}


StreamItem.contextTypes = {
    store: PropTypes.object
};

StreamItem.defaultProps = {
    expanded: false,
    onDel(){
    },
    onEdit(){
    },
    onSubscribe(){
    },
    onUnsubscribe(){
    },
    prev: false
};

StreamItem.propsTypes = {
    stream: PropTypes.object.isRequired,
    onEdit: PropTypes.func,
    onSubscribe: PropTypes.func,
    onUnsubscribe: PropTypes.func,
    onDel: PropTypes.func,
    onClick: PropTypes.func.isRequired,
    expanded: PropTypes.bool
};