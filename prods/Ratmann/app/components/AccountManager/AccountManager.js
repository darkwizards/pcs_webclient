import React, {Component, PropTypes} from "react";
import AccountDropdown from "../../../../../appComponents/components/AccountDropdown/AccountDropdown";
import {Nav} from "react-bootstrap";
import {Link} from "react-router";


export default class AccountManager extends Component {

    constructor(props) {

        super(props);

    }

    render() {

        return (
            ~this.props.currentAccount.id ? (
                    <AccountDropdown currentAccount={ this.props.currentAccount }/>
                ) : (
                    <Nav pullRight>
                        <li>
                            <Link to="/register">Регистрация</Link>
                        </li>
                    </Nav>
                )
        )

    }

}


AccountManager.propTypes = {
    currentAccount: PropTypes.object
};