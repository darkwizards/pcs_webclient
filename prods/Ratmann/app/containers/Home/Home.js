import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import React, {Component} from "react";
import {Link} from "react-router";
import Login from "../../../../../appComponents/components/RequireLogin/RequireLogin";
import {version} from "../../../../../package.json";
import {apiServer} from "../../../../../config/settings.json";
import {notify} from "../../../../../appComponents/actions/notice";


class Home extends Component {

    constructor(props) {

        super(props);

        this.state = {
            users: [],
            filters: [],
            action: null,
            current: -1
        };

    }

    render() {
        return (
            <Login>
                <div className="col-xs-12">
                    <div className="row">
                        <h2 className="text-center">VerQ Admin Panel</h2>

                        <ul>
                            <li>
                                <Link className="h6" to="/admin/users">
                                    Пользователи ({this.props.users.length})
                                </Link>
                            </li>
                            <li>
                                <Link className="h6" to="/admin/reports">
                                    Отчёты
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </Login>
        )


    }

}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        notify
    }, dispatch)
}

export default connect(state => ({users: state.users}), mapDispatchToProps)(Home)