import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import React, {Component} from "react";
import Login from "../../../../../appComponents/components/RequireLogin/RequireLogin";
import {version} from "../../../../../package.json";
import {apiServer} from "../../../../../config/settings.json";
import Modal from "react-bootstrap/lib/Modal";
import {fetchAll, editUser, createUser, removeUser} from "../../../../../appComponents/actions/users";
import {notify} from "../../../../../appComponents/actions/notice";
import Create from "../../components/UserForm/UserForm";
import Row from "../../components/UserItem/UserItem";
import Find from "lodash/find";


class Home extends Component {

    constructor(props) {

        super(props);

        this.state = {
            users: [],
            filters: [],
            action: null,
            current: -1
        };

    }

    render() {

        let
            rows = this.props.users.map(usr => (
                <Row
                    onEdit={() => this.setState({
                        current: usr.id,
                        action: 'edit'
                    })}
                    onDel={() => this.props.remove(usr.id)}
                    key={usr.id}
                    user={usr}
                />
            ));

        // TODO решить проблему с таблицей на смартах
        return (
            <Login>
                <div
                    className="col-xs-12 text-center"
                >
                    <Modal
                        bsSize="large" show={!!this.state.action}
                        onHide={() => this.setState({action: null, current: -1})}
                    >
                        <Modal.Header>
                            <Modal.Title>
                                {this.state.action == 'edit' ? 'Изменение данных' : 'Создание нового'} пользователя
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Create
                                defaults={Find(this.props.users, {id: this.state.current}) || {}}
                                onSave={data => {

                                    this.props[this.state.action](data);

                                    this.setState({action: null, current: -1});

                                }}
                                onCancel={() => this.setState({action: null, current: -1})}
                            />
                        </Modal.Body>
                    </Modal>
                    <h3 className="text-center col-xs-12">Пользователи</h3>
                    <button
                        className="btn btn-success col-xs-12 col-md-4 col-md-push-4"
                        onClick={() => this.setState({action: 'create'})}
                    >
                        Создать нового пользователя
                    </button>
                    <p className="col-xs-12"/>
                    <div className="row">
                        <div className="table-responsive col-xs-12">
                            <table className="table table-stripped table-bordered">
                                <tbody className="">
                                <tr>
                                    <th>Имя</th>
                                    <th>Почта</th>
                                    <th>Тип</th>
                                    <th>Специализация</th>
                                </tr>
                                {rows}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </Login>
        )

    }

}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        reload: fetchAll,
        edit: editUser,
        create: createUser,
        remove: removeUser,
        notify
    }, dispatch)
}

export default connect(state => ({users: state.users}), mapDispatchToProps)(Home)