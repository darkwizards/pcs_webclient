import React, {Component} from "react";
import ReactDOM from "react-dom";
import configureStore from "./store/configureStore";
import {Provider} from "react-redux";
import AppRouter from "./routes";
import {fetchAll as users} from "../../../appComponents/actions/users";
import fetcher from "../../../appComponents/modules/AutoFetcher";
import {SESSION_RESUMPTION} from "../../../appComponents/actions/profile";
import {setStore} from "../../../appComponents/modules/ErrorHandler";


// if (!window.Promise)
window.Promise = require("promise-polyfill");


const
    initialState = {},
    store = configureStore(initialState);


setStore(store);


store.dispatch({
    type: SESSION_RESUMPTION
});


fetcher('register', users);
fetcher('set store', store);
fetcher('start');
fetcher();


class Root extends Component {

    render() {

        return (
            <Provider store={store}>
                <AppRouter />
            </Provider>
        )

    }

}


let rootElement = document.getElementById('root');


ReactDOM.render(
    <Root />, rootElement
);