export const ADD_MEET = 'ADD_QUE';
export const REQ_ADD_MEET = 'REQ_ADD_QUE';
export const EDIT_MEET = 'UPDATE_QUE';
export const REQ_EDIT_MEET = 'REQ_UPDATE_QUE';
export const RESTART_MEETS = 'RESTART_QUE';
export const DEL_MEET = 'DEL_QUE';
export const REQ_DEL_MEET = 'REQ_DEL_QUE';
export const REQ_FETCH_MEETS = 'REQ_FETCH_MEETS';
export const REQ_START_MEET = 'REQ_START_QUE';
export const REQ_END_MEET = 'REQ_END_QUE';


import {apiServer} from "../../config/settings.json";


/**
 * @param {Object} data
 * @return {{type: string, data: *}}
 */
export function editMeet(data) {
    return {
        type: REQ_EDIT_MEET,
        data
    }
}

/**
 *
 * @return {{type: string}}
 */
export function fetchAll() {
    return {
        type: REQ_FETCH_MEETS
    }
}

/**
 * @param {String} stream_id
 * @param {Number} duration
 * @param {Number} min_time
 * @return {{type: string, data: {duration: *, min_time: *, stream_id: *}}}
 */
export function createMeet({stream_id, duration, min_time}) {
    return {
        type: REQ_ADD_MEET,
        data: {
            duration,
            min_time,
            stream_id
        }
    }
}

/**
 * @param {String} id
 * @return {{type: string, data: Object}}
 */
export function deleteMeet(id) {
    return {
        type: REQ_DEL_MEET,
        data: id
    }
}

/**
 * @param {String} id
 * @return {{type: string, data: *}}
 */
export function startMeet(id) {
    return {
        type: REQ_START_MEET,
        data: id
    }
}

/**
 * @param {String} id
 * @return {{type: string, data: *}}
 */
export function endMeet(id) {
    return {
        type: REQ_END_MEET,
        data: id
    }
}
