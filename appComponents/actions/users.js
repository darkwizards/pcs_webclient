export const ADD_USER = 'ADD_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const RESTART_USERS = 'RESTART_USERS';
export const DEL_USER = 'DEL_USER';


import {notify} from "./notice";
import Users from "../../prods/Ratmann/app/caches/UsersCache";
import Handel from "../modules/ErrorHandler";
import {apiServer} from "../../config/settings.json";
import count from "../modules/RusNum";
import Diff from "../modules/Diff";
import md5 from "js-md5";


export function editUser(user) {
    return function (dispatch, State) {

        let sess = State().session;

        if (!~sess.user.id)
            return;

        if (user.pass == "=!=")
            delete user.pass;

        else
            user.pass = md5(user.pass);

        Users
            .edit(user.id, {
                ...user,
                children: user.children + ''
            }, sess.token)
            .then(function (data) {

                dispatch({
                    type: UPDATE_USER,
                    data
                });

                dispatch(notify({
                    type: 0,
                    text: "Данные пользователя изменены"
                }))

            })
            .catch(function (error) {
                console.log(error);

                dispatch(notify({
                    type: 3,
                    text: "Ошибка изменения пользователя"
                }));

                return Handel(error, dispatch);

            })

    }
}

export function fetchAll() {
    return function (dispatch, State) {

        let sess = State().session;

        if (!~sess.user.id)
            return;

        Users
            .fetch(sess.token)
            .then(/** @param {Array} data */function (data) {

                let diff = Diff(State().users, data);

                diff.deleted.forEach(function (data) {

                    dispatch({
                        type: DEL_USER,
                        data
                    })

                });

                diff.created.forEach(function (data) {

                    dispatch({
                        type: ADD_USER,
                        data
                    })

                });

                diff.changed.forEach(function (data) {

                    dispatch({
                        type: UPDATE_USER,
                        data
                    })

                });

                if (diff.deleted.length + diff.created.length + diff.changed.length)
                    dispatch(notify({
                        type: 0,
                        text: [

                            diff.deleted.length ?
                                "Удалено " + diff.deleted.length + count(" встреч_а_и_", diff.deleted.length) :

                                null,

                            diff.created.length ?
                                "Получено " + diff.created.length + count(" нов_ый_ых_ых", diff.created.length) +
                                count(" пользовател_ь_я_ей", diff.created.length) :

                                null,

                            diff.changed.length ?
                                "Изменено " + diff.changed.length + count(" пользовател_ь_я_ей", diff.changed.length) :

                                null

                        ].filter(el => el).join(" ")
                    }))

            })
            .catch(function (error) {

                console.dir(error);

                dispatch(notify({
                    type: 3,
                    text: "Ошибка получения списка встреч"
                }));

                return Handel(error, dispatch);

            })
    }
}

export function createUser({mail, pass, children, name, status, type}) {
    return function (dispatch, State) {

        let sess = State().session;

        if (!~sess.user.id)
            return;

        pass = md5(pass);

        Users
            .create({
                mail,
                pass,
                children: children.join(","),
                name,
                status: type == 1 ? "Родитель" : status,
                type
            }, sess.token)
            .then(function (data) {

                dispatch({
                    type: ADD_USER,
                    data
                });

                dispatch(notify({
                    text: "Новая пользователь создан"
                }))

            })
            .catch(function (error) {

                console.dir(error);

                dispatch(notify({
                    type: 3,
                    text: "Ошибка создания пользователя"
                }));

                return Handel(error, dispatch);

            })
    }
}

export function removeUser(id) {
    return function (dispatch, State) {

        let sess = State().session;

        if (!~sess.user.id)
            return;

        Users
            .remove(id, sess.token)
            .then(function () {

                dispatch({
                    type: DEL_USER,
                    data: {
                        id
                    }
                });

                dispatch(notify({
                    type: 3,
                    text: "Пользователь удалён"
                }))

            })
            .catch(function (error) {

                console.dir(error);

                dispatch(notify({
                    type: 3,
                    text: "Ошибка удаления пользователя"
                }));

                return Handel(error, dispatch);

            })

    }
}