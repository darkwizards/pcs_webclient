/**
 * Created by banonotit on 27.11.16.
 */

export const ERROR_STREAMS = "ERR_STEAM";
export const ADD_STREAM = "ADD_STREAM";
export const REMOVE_STREAM = "RM_STREAM";
export const EDIT_STREAM = "EDIT_STREAM";
export const RESTART_STREAM = "RESTART_STREAM";
export const REQUEST_STREAM_CREATION = "REQ_STREAM_CREATE";
export const REQUEST_STREAM_FETCH = "REQ_STREAM_FETCH";
export const REQUEST_STREAM_EDIT = "REQ_STREAM_EDIT";
export const REQUEST_STREAM_REM = "REQ_STREAM_REM";


/**
 * @param {Number} duration
 * @param {String} description
 * @param {Number|Date} time_start
 * @param {Number|Date} time_end
 * @param {Boolean} repeat
 */
export function add({duration, description = "", time_start = new Date(), time_end = new Date(), repeat = false}) {
    return {
        type: REQUEST_STREAM_CREATION,
        data: {
            time_start: +time_start,
            time_end: +time_end,
            desc: description,
            duration,
            repeat: +Boolean(repeat) + ""
        }
    }
}

export function fetchAll() {
    return {
        type: REQUEST_STREAM_FETCH
    }
}

export function edit({id, duration, description = "", time_start = new Date(), time_end = new Date(), repeat = false}) {
    return {
        type: REQUEST_STREAM_EDIT,
        data: {
            id,
            time_start: +time_start,
            time_end: +time_end,
            description,
            duration,
            repeat: +Boolean(repeat) + ""
        }
    }
}

export function remove(data) {
    return {
        type: REQUEST_STREAM_REM,
        data
    }
}