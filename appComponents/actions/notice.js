/**
 * Created by banonotit on 23.11.16.
 */

export const ADD_NOTIFY = 'ADD_NOTIFY';
export const RM_NOTIFY = 'RM_NOTIFY';

/**
 * @param {{type: number?, text: string, expire: number?}} data
 * @return {{type: string, data: *}}
 */
export function notify(data) {
    return {
        type: ADD_NOTIFY,
        data
    }
}
export function rmNotif(data) {
    return {
        type: RM_NOTIFY,
        data
    }
}