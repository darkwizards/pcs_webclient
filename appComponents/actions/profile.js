export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_RESPONSE = 'LOGIN_RESPONSE';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGOUT = 'LOGOUT';
export const REGISTER_REQUEST = 'REG_REQ';
export const REGISTER_ERROR = 'REG_ERR';
export const USER_DATA_RECIVE = 'USR_DAT_RECIVE';
export const SESSION_RESUMPTION = 'SESS_RESUMP';


import axios from "axios";
import {apiServer as apiUrl} from "../../config/settings.json";
import md5 from "js-md5";
import qs from "querystring";
import Handel from "../modules/ErrorHandler";
import {notify} from "./notice";


export function login({mail, pass}) {
    // alert(arguments);

    pass = md5(pass);

    return (dispatch, State) => {

        let sess = State().session;

        if (~sess.user.id)
            return;

        dispatch({
            type: LOGIN_REQUEST
        });

        // alert("Login...");
        axios
            .post(apiUrl + '/login', qs.stringify({mail, pass}))
            .then(function ({data}) {

                dispatch({
                    type: LOGIN_RESPONSE,
                    data
                });

            })
            .catch(function (error) {

                dispatch({
                    type: LOGIN_ERROR,
                    error
                });

                return Handel(error, dispatch)

            })
    }
}

export function logout() {
    return {
        type: LOGOUT
    }
}

/**
 * @param {string} name
 * @param {Array.<String>} children
 * @param {string} mail
 * @param {string} pass
 * @param {number} type
 * @param {string} status
 */
export function register({name, children, mail, pass, type = 0, status}) {

    pass = md5(pass);

    return (dispatch, State) => {

        let sess = State().session;

        if (~sess.user.id)
            return;

        dispatch({
            type: REGISTER_REQUEST
        });

        axios
            .post(apiUrl + '/register', qs.stringify({
                name,
                mail,
                pass,
                type,
                status,
                children: children.join(",")
            }))
            .then(function ({data}) {

                // TODO сделать с нормальным роутером, а не с этой форсой.
                if (data === true)
                    location.href = "/mail_request"

            })
            .catch(function (err) {

                dispatch({
                    type: REGISTER_ERROR,
                    err
                });


                dispatch(notify({
                    text: "Пользователь с таким email уже зарегистрирован",
                    type: 3
                }));

                console.log(err);

                return Handel(err, dispatch)

            })

    }
}