/**
 * Created by banonotit on 27.11.16.
 */
import {ADD_STREAM, REMOVE_STREAM, EDIT_STREAM, RESTART_STREAM} from "../actions/streams";
import objectsEqual from "lodash/isEqual";


const defaultState = [];


export default function (state = defaultState, action) {

    switch (action.type) {
        case RESTART_STREAM:

            return defaultState.slice();

        case ADD_STREAM:

            return state.slice().concat({
                ...action.data
            });

        case REMOVE_STREAM:

            return state.filter(el => !objectsEqual(el, {...el, ...action.data}));

        case EDIT_STREAM:

            return state.map(el => el.id != action.data.id ? el : {...el, ...action.data});

        default:

            return Array.isArray(state) ? state.slice() : {...state};

    }

}