/**
 * Created by banonotit on 23.11.16.
 */
import * as Actions from "../actions/notice";
import objectsEqual from "lodash/isEqual";


const defaultState = [
    {
        id: 0,
        text: "Здравствуйте!",
        type: 0,
        expire: 15e3
    }
];


export default function (state = defaultState, action) {

    switch (action.type) {
        case Actions.ADD_NOTIFY:

            return state
                .slice()
                .concat({
                    ...defaultState[0],
                    ...action.data,
                    id: state.length
                });

        case Actions.RM_NOTIFY:

            return state
                .filter(elem => !objectsEqual(
                    elem,
                    // создаём копию проверяемого элемента, и изменяем в неём те свойства, по которым ищем
                    {...elem, ...action.data}
                ));

        default:

            return state;

    }

}
