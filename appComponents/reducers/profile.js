import * as Actions from "../actions/profile";
import {apiServer} from "../../config/settings.json";


export const defaultState = {
    user: {
        id: -1,
        name: '',
        children: "",
        type: 0,
        perm: {
            id: -1,
            status_name: '',
            mask: 28951
        }
    },
    token: undefined
};


export default function profile(state = defaultState, action) {

    switch (action.type) {
        case Actions.LOGIN_RESPONSE:

            let
                {token} = action.data;

            return {
                ...state,
                token
            };

        case Actions.USER_DATA_RECIVE:

            return {
                ...state,
                user: action.data
            };

        case Actions.LOGOUT:

            return {...defaultState};

        default:

            return {...state};

    }

}
