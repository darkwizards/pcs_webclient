/**
 * Created by banonotit on 27.11.16.
 */
import {ADD_MEET, DEL_MEET, EDIT_MEET, RESTART_MEETS} from "../actions/meets";
import objectsEqual from "lodash/isEqual";


const defaultState = [];


export default function (state = defaultState, action) {

    switch (action.type) {
        case RESTART_MEETS:

            return defaultState.slice();

        case ADD_MEET:

            return state.slice().concat({
                ...action.data
            });

        case DEL_MEET:

            return state.filter(el => !objectsEqual(el, {...el, ...action.data}));

        case EDIT_MEET:

            return state.map(el => el.id != action.data.id ? el : {...el, ...action.data});

        default:

            return state;

    }

}