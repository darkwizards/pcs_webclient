/**
 * Created by banonotit on 10.01.17.
 */
import intersectionBy from "lodash/intersectionBy";
import without from "lodash/without";
import diff from "lodash/differenceWith";
import isEqual from "lodash/isEqual";


/**
 * @param {[{id:string}]} old
 * @param {[{id:string}]} cur
 * @return {{changed:[{}],deleted:[{}],created:[{}]}}
 */
export default function (old, cur) {

    let
        inNew = intersectionBy(cur, old, "id"),
        inOld = intersectionBy(old, cur, "id"),
        deleted = without(old, ...inOld),
        created = without(cur, ...inNew),
        changed = diff(inNew, inOld, isEqual);

    return {
        changed,
        deleted,
        created
    }

}