/**
 * Created by banonotit on 04.01.17.
 */
import {notify} from "../actions/notice";
import {logout} from "../actions/profile";
import {fetchAll as streams} from "../actions/streams";
import {fetchAll as meets} from "../actions/meets";


let store = undefined;

export function setStore(s) {
    store = s
}

export default function (error, dispatch) {
    console.log(error, 'logger');
    if (error.response.data) {
        switch (error.response.data.error) {
            case 0:
                dispatch(logout());
                dispatch(notify({
                    type: 3,
                    text: "Сервер отверг сессию. Необходимо заново войти."
                }));

                return Promise.reject(error);

            case 1:
                dispatch(notify({
                    type: 3,
                    text: "Не хватает прав для выполнения этого действия"
                }));

                return Promise.reject(error);

            case 2:
                dispatch(notify({
                    type: 3,
                    text: "Ошибка вызова методов. Сообщите разработчику о последних действиях, проведённых Вами."
                }));

                return Promise.reject(error);

            case 3:
                dispatch(notify({
                    type: 3,
                    text: "Такого пользователя не существует в системе. Возможно не был подтверждён email."
                }));

                return Promise.reject(error);

            case 4:
                dispatch(notify({
                    type: 3,
                    text: "Ошибка входа в систему, введён неверный пароль."
                }));

                return Promise.reject(error);

            case 7:
                dispatch(streams());
                dispatch(meets());

                dispatch(notify({
                    type: 3,
                    text: "Такого объекта нету на сервере. Кеш перезагружается..."
                }));

                return Promise.reject(error);

            default:
                return Promise.reject(error);

        }
    }
    return Promise.reject(error);
}