/**
 * Created by banonotit on 22.12.16.
 */
import axios from "axios";
import Cache from "./Cache";
import qs from "querystring";
import {apiServer} from "../../config/settings.json";


/**
 * @class CacheManager
 * @memberOf VerQ
 * @template T
 */
export default class ModelStore {

    /**
     * @param {String} [create]
     * @param {String} [remove]
     * @param {String} [edit]
     * @param {String} [fetch]
     * @param {String} get
     * @param {number} [cacheTime]
     */
    constructor({create, remove, edit, get, fetch}, cacheTime = 10000) {

        this._cache = new Cache(cacheTime);
        this.urls = {create, remove, edit, get, fetch};
        this._req = axios.create({
            baseURL: apiServer
        })

    }

    /**
     * @param id
     * @param token
     * @return {Promise.<T>|axios.Promise}
     */
    get(id, token) {

        if (!this.urls.get)
            return Promise.reject(new Error("No url for get specified"));

        let a = this._cache.get(id);

        if (a === undefined)
            return this._req
                .get(this.urls.get + "?" + qs.stringify({token, id}))
                .then(({data}) => {

                    this._cache.set(id, data);

                    return Promise.resolve(data)

                });
        else
            return Promise.resolve(a)

    }

    /**
     * @param id
     * @param data
     * @param token
     * @return {Promise<{}>|axios.Promise}
     */
    edit(id, data, token) {

        if (!this.urls.edit)
            return Promise.reject(new Error("No url for edit specified"));

        return this.get(id, token)
            .then(elem =>
                this._req.post(this.urls.edit, qs.stringify({...elem, ...data, token}))
                    .then(({data}) => {

                        this._cache.set(data.id, data);

                        return Promise.resolve(data);

                    })
            )

    }

    /**
     * @param data
     * @param token
     * @return {axios.Promise|Promise.<{}>}
     */
    create(data, token) {

        if (!this.urls.create)
            return Promise.reject(new Error("No url for create specified"));

        return this._req.post(this.urls.create, qs.stringify({...data, token}))
            .then(({data}) => {

                this._cache.set(data.id, data);

                return Promise.resolve(data)

            })

    }

    /**
     * @param {number} id
     * @param {string} token
     * @return {axios.Promise|Promise}
     */
    remove(id, token) {

        if (!this.urls.remove)
            return Promise.reject(new Error("No url for remove specified"));

        return this._req.post(this.urls.remove, qs.stringify({id, token}))
            .then(() => {

                this._cache.remove(id);

                return Promise.resolve()

            })

    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * @return {[string]}
     */
    get keys() {
        return this._cache.keys
    }

    /**
     * @param {String} token
     * @return {axios.Promise<[{}]>|Promise.<[{}]>}
     */
    fetch(token) {

        if (!this.urls.fetch)
            return Promise.reject(new Error("No url for fetch specified"));

        this._cache.reset();

        return this._req.get(this.urls.fetch + "?" + qs.stringify({token}))
            .then(({data}) => {

                data.forEach(stream => this._cache.set(stream.id, stream));

                return Promise.resolve(data);

            })

    }

}