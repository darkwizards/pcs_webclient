/**
 * copypasted by BANO from https://github.com/lassombra/reactive-storage and edited
 */
import find from "lodash/find";

export default class CookieStore {

    constructor(duration) {

        if (duration) {
            this.duration = duration;
        }

        // cache contents because sometimes cookie changes are slow to propogate.
        // We want to make sure that we have everything cached and that cookies exist
        // only to map data between tabs and between sessions if duration was set.
        this.storedMap = {};
    }

    getItem(name) {

        if (this.storedMap.hasOwnProperty(name)) {

            return this.storedMap[name];

        } else {

            let
                cookies = document.cookie.split('; '),
                cookieParsed = cookies.map(cookie => {

                    let pieces = cookie.split('=');

                    return {key: pieces.shift(), value: pieces.join('=')};

                }),
                cookie = find(cookieParsed, cookie => cookie.key === name);

            if (cookie) {

                // cache it so that we don't have to go through this process again anytime soon.
                this.storedMap[name] = cookie.value;

                return cookie.value;

            }
        }
    }

    setItem(name, value) {

        this.storedMap[name] = value;

        let age;

        if (this.duration)//max-age is specified in seconds.  This converts duration to seconds.
            age = `; max-age=${this.duration * 60 * 60 * 24}`;

        else
            age = '; expires=';


        document.cookie = `${name}=${value}; path=/${age}`;

    }

    removeItem(name) {

        this.storedMap[name] = undefined;

        document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 GMT`;

    }

    reload() {

        this.storedMap = {};

    }

};