/**
 * Created by banonotit on 03.01.17.
 */


let state = {
    actions: []
};


function fetch() {

    if (!state.listening || !state.store || !state.actions.length)
        return;

    state.actions.forEach(n => state.store.dispatch(n()));

}


export default function singelton(action, data) {
    switch (action) {
        case 'set store':

            state.store = data;

            break;

        case 'register':

            state.actions.push(data);

            break;

        case 'unregister':

            state.actions.filter(a => a !== data);

            break;

        case 'start':

            state.listening = true;

            break;

        case 'stop':

            state.listeinig = false;

            break;

        default:

            fetch();

            break
    }
}


setInterval(fetch, 6e4);
