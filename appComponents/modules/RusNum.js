/**
 * @author BANO.notIT's Bairn
 * @param {string}w
 * @param {number}n
 * @return {string}
 */
export default function end125(w, n) {
    w = w.split("_");
    n = (n %= 100) < 15 && n > 9 && (6) || n % 10;
    return n === 1 && (w[0] + w[1])
        || n < 5 && n && (w[0] + w[2])
        || (w[0] + w[3])
}

export const ready = {
    files: "файл__а_ов",
    time: {
        secs: "секунд_а_ы_",
        mins: "минут_а_ы_",
        hours: "час__а_ов",
        days: "д_ень_ня_ней",
        years: "_год_года_лет",
        weeks: "недел_я_и_ь",
        months: "месяц__а_ев"
    },
    chars: "букв_а_ы_",
    words: "слов_о_а_",
    humans: "_человек_человека_людей",
    men: "мужчин_а__",
    women: "женщин_а_ы_",
    баллы: "балл__а_ов"
};