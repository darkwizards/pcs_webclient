/**
 * Created by banonotit on 15.12.16.
 */

export default class Cache {
    constructor(defaultExpire = null) {
        this._data = {};
        this.defaultExpire = defaultExpire;
    }

    reset() {
        this._data = {};
    }

    get keys() {
        return Object.keys(this._data);
    }

    get(key) {
        if (this._data.hasOwnProperty(key) && this._data[key].expire > +new Date())
            return this._data[key].data;
        else
            return undefined;
    }

    set(key, data, expire = this.defaultExpire || +new Date() + 1e5) {
        this._data[key] = {
            expire,
            data
        }
    }

    remove(key) {
        delete this._data[key]
    }
}