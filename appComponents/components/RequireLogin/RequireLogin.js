import React, {Component, PropTypes} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {push} from "redux-router";
import * as Actions from "../../actions/profile";
import LoginForm from "../LoginForm/LoginForm";

class RequireLogin extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (~this.props.user.id)
            if (this.props.single) {
                setTimeout(() => this.props.push('/'), 0);
                return null
            } else
                return this.props.children;

        return (
            <div className="col-md-6 col-md-push-3">
                {
                    <h5 className="text-center alert alert-info">Авторизация</h5>
                }
                <LoginForm onLoginClick={this.props.login}/>
            </div>
        )
    }
}
RequireLogin.contextTypes = {
    store: PropTypes.object
};
RequireLogin.propTypes = {
    user: PropTypes.object,
    single: PropTypes.bool
};
function mapStateToProps(state) {
    let user = state.session.user;
    return {
        user,
        router: state.router
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({login: Actions.login, push}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(RequireLogin);
