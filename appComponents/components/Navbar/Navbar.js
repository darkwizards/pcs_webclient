import React, {Component, PropTypes} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {version} from "../../../package.json";
import AccountManager from "../AccountManager/AccountManager";
import Nav from "react-bootstrap/lib/Navbar";
import "./Navbar.scss";


class AppNav extends Component {

    constructor(props) {

        super(props);

    }

    render() {

        return (
            <Nav inverse fixedTop>
                <Nav.Header>
                    <Nav.Brand>
                        <a href="/">
                            VerQ
                            <sup
                                className="badge"
                                dangerouslySetInnerHTML={{
                                    __html: version
                                        .replace("alpha", "&alpha;")
                                        .replace("beta", "&beta;")
                                }}
                            />
                        </a>
                    </Nav.Brand>
                    <Nav.Toggle/>
                </Nav.Header>
                <Nav.Collapse>
                    <AccountManager currentAccount={ this.props.user }/>
                </Nav.Collapse>
            </Nav>
        )

    }
}


AppNav.propTypes = {
    account: PropTypes.object
};


function mapStateToProps(state) {
    let user = state.session.user;
    return {
        user,
        router: state.router
    };
}

//Place action methods into props
function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(AppNav);
