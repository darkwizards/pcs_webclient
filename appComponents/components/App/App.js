import React, {Component, PropTypes} from "react";
import "./App.scss";
import Navbar from "../Navbar/Navbar";
import Notify from "../NotifyContainer/NotifyContainer";


class Main extends Component {

    constructor(props) {

        super(props);

    }

    render() {

        return (
            <div className="app">
                <Navbar />
                <div className="container">
                    <div className="row">
                        <Notify />
                        {this.props.children}
                    </div>
                </div>
            </div>
        )

    }
}


Main.contextTypes = {
    store: PropTypes.object
};


export default Main;
