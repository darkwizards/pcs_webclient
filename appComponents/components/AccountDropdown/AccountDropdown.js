import React, {Component, PropTypes} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as Actions from "../../actions/profile";
import Nav from "react-bootstrap/lib/Nav";
import {Link} from "react-router";


class AccountDropdown extends Component {

    render() {

        return (
            <Nav pullRight>
                <li>
                    <a href="mailto:verq@vertical1748.ru">Поддержка</a>
                </li>
                {
                    this.props.que ?
                        [(
                            <li>
                                <Link to="/queue">
                                    Встречи <sup className="badge">{this.props.que.length}</sup>
                                </Link>
                            </li>
                        ), (
                            <li>
                                <Link to="/stream">
                                    {
                                        'type' in this.props.user && this.props.user.type == 1 ?
                                            'Учителя' :
                                            'Очереди'
                                    }
                                </Link>
                            </li>
                        )] :
                        null
                }
                <li>
                    <Link to="/" onClick={() => this.props.logout()}>
                        <span>Выход</span>
                    </Link>
                </li>
            </Nav>
        )

    }

}


AccountDropdown.propTypes = {
    currentAccount: PropTypes.object.isRequired
};


function mapStateToProps(state) {
    // console.log(state);

    let
        user = state.session.user;

    return {
        user,
        que: state.meets,
        msg: state.messages,
        router: state.router
    };

}

//Place action methods into props
function mapDispatchToProps(dispatch) {

    return bindActionCreators(Actions, dispatch);

}


export default connect(mapStateToProps, mapDispatchToProps)(AccountDropdown);