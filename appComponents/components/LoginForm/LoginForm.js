import React, {Component, PropTypes} from "react";
import {push} from "redux-router";
import Collapse from "react-bootstrap/lib/Collapse";
import "./LoginForm.scss";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

class LoginForm extends Component {
    constructor(props) {

        super(props);

        this.handleLogin = this.handleLogin.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.regBtn = this.regBtn.bind(this);

        this.state = {mail: '', pass: '', log: false};

    }

    /**
     * @function handleLogin
     * @description Fire onLoginClick function provided to component when login is clicked
     */
    handleLogin(event) {

        event.preventDefault();

        if (!this.state.log)
            this.setState({log: true});

        else if (this.state.mail && this.state.pass)
            this.props.onLoginClick({mail: this.state.mail, pass: this.state.pass});

    }

    regBtn(ev) {

        ev.preventDefault();

        if (this.state.log)
            this.setState({log: false});

        else
            this.props.push("/register");

    }

    /**
     * @function handleUsernameChange
     * @description Update the state with the values from the form inputs.
     * @fires context#setState
     */
    handleUsernameChange(event) {

        this.setState({
            mail: event.target.value
        });

    }

    handlePasswordChange(event) {

        this.setState({
            pass: event.target.value
        })

    }

    render() {
        return (
            <div
                className="login-form"
            >
                <form
                    onSubmit={this.handleLogin}
                >
                    <Collapse in={this.state.log}>
                        <div>
                            <label className="control-label center-block text-center h6">Email</label>
                            <div
                                className="form-group"
                            >
                                <input
                                    type="email"
                                    className="form-control"
                                    onChange={this.handleUsernameChange}
                                    placeholder="my_mail@example.com"
                                    id="login-email"
                                />
                                <label
                                    className="login-field-icon fui-mail"
                                    htmlFor="login-name"
                                />
                            </div>
                            <label className="control-label center-block text-center h6">Пароль</label>
                            <div className="form-group">
                                <input
                                    type="password" className="form-control"
                                    onChange={this.handlePasswordChange}
                                    placeholder="*****"
                                    id="login-pass"
                                />
                                <label className="login-field-icon fui-lock" htmlFor="login-pass"/>
                            </div>
                        </div>
                    </Collapse>
                    <button className={"btn btn-success btn-lg btn-block " + (this.state.log ? "form-group" : "")}>
                        Вход
                    </button>
                    <Collapse in={!this.state.log}>
                        <div>
                            <div className="h6 text-center">или</div>
                        </div>
                    </Collapse>
                    <button
                        onClick={this.regBtn}
                        className="btn btn-block btn-lg btn-info"
                    >
                        {this.state.log ? "Назад" : "Регистрация"}
                    </button>
                </form>
            </div>
        )
    }
}
LoginForm.propTypes = {
    onLoginClick: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        push
    }, dispatch);
}


export default connect(() => ({}), mapDispatchToProps)(LoginForm);