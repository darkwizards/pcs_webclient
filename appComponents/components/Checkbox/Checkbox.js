import React, {PropTypes, Component} from "react";
import "./Checkbox.scss";

let counter = -1;

export default class Checkbox extends Component {
    constructor(props) {

        super(props);

        this.counter = ++counter;

        this.handleChange = this.handleChange.bind(this);


    }

    handleChange() {

        this.props.onChange(!this.props.checked)

    }


    render() {
        return (
            <label
                htmlFor={'ver-checkbox-' + this.counter}
                className={"ver-checkbox " + this.props.className}
            >
                <input
                    type="checkbox" id={'ver-checkbox-' + this.counter}
                    onChange={this.handleChange}
                    checked={this.props.checked}
                />
                <span>{this.props.children}</span>
            </label>
        )
    }
}

Checkbox.defaultProps = {
    checked: false,
    onChange: (a) => a,
    className: ''
};

Checkbox.propTypes = {
    checked: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
    className: PropTypes.string
};