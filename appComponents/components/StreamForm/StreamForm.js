import React, {Component, PropTypes} from "react";
import RMinuts, {ready} from "../../modules/RusNum";
import Equal from "lodash/isEqual";
import MaskedInput from "react-maskedinput";
import Checkbox from "../Checkbox/Checkbox";
import moment from "moment";
moment.locale("ru");


class StreamCreateForm extends Component {

    constructor(props) {

        super(props);

        console.log(moment(props.defaults.time_end).format("HH:mm"));

        this.state = {
            avg_time_focus: false,
            defaults: props.defaults,
            description: "",
            duration: 5,
            repeat: 0,
            ...props.defaults,
            time_start: props.defaults.time_start ? moment(props.defaults.time_start).format("DD\.MM\.YYYY HH:mm") : "",
            time_end: props.defaults.time_end ? moment(props.defaults.time_end).format("HH:mm") : "",
        };

    }

    save() {

        if (
            !/^(?:[0-2]\d|3[01])\.(?:0[1-9]|1[0-2])\.20\d\d (?:[01]\d|2[0-3]):[0-5]\d$/
                .test(this.state.time_start) || !/^(?:[01]\d|2[0-3]):[0-5]\d$/
                .test(this.state.time_end)
        )
            return false;

        console.log('state', this.state);

        let
            fromDef = {
                time_start: new Date(this.state.defaults.time_start),
                time_end: new Date(this.state.defaults.time_end),
                description: this.state.defaults.description,
                duration: this.state.defaults.duration,
                repeat: !!this.state.defaults.repeat
            },
            res = {
                time_start: moment(this.state.time_start, "DD\.MM\.YYYY HH:mm", true),
                time_end: moment(this.state.time_start.split(" ")[0] + " " + this.state.time_end, "DD\.MM\.YYYY HH:mm", true),
                description: this.state.description,
                duration: this.state.duration,
                repeat: !!this.state.repeat
            };


        if (!Equal(res, fromDef))
            this.props.onSave(res);

        else
            this.props.onCancel()

    }

    render() {
        return (
            <form
                onSubmit={ev => {
                    ev.preventDefault();
                    this.save();
                }}
            >
                <div className="row">

                    <div
                        className="form-group col-md-6 col-xs-12"
                    >
                        <label className="control-label">Дата и время начала приёма</label>
                        <MaskedInput
                            mask="R1.O1.\2\011 T1:F1"
                            value={this.state.time_start}
                            onChange={({target}) =>
                                this.setState({time_start: target.value})
                            }
                            className={
                                "form-control " +
                                (
                                    !this.state.time_start ||
                                    /^(?:[0-2]\d|3[01])\.(?:0[1-9]|1[0-2])\.20\d\d (?:[01]\d|2[0-3]):[0-5]\d$/.test(this.state.time_start) ?
                                        "" :
                                        "has-error"
                                )
                            }
                            formatCharacters={{
                                "O": {
                                    validate(char){
                                        return (+char) < 2
                                    },
                                    transform(char){
                                        return char
                                    }
                                },
                                "T": {
                                    validate(char){
                                        return (+char) < 3
                                    },
                                    transform(char){
                                        return char
                                    }
                                },
                                "R": {
                                    validate(char){
                                        return (+char) < 4
                                    },
                                    transform(char){
                                        return char
                                    }
                                },
                                "F": {
                                    validate(char){
                                        return (+char) < 6
                                    },
                                    transform(char){
                                        return char
                                    }
                                }
                            }}
                        />
                    </div>
                    <div className="form-group col-md-6 col-xs-12">
                        <label className="control-label">Время конца</label>
                        <MaskedInput
                            mask="T1:F1"
                            value={this.state.time_end}
                            onChange={({target}) =>
                                this.setState({time_end: target.value})
                            }
                            className={
                                "form-control " +
                                (
                                    !this.state.time_end ||
                                    /^(?:[01]\d|2[0-3]):[0-5]\d$/.test(this.state.time_end) ?
                                        "" :
                                        "has-error"
                                )
                            }
                            formatCharacters={{
                                "T": {
                                    validate(char){
                                        return (+char) < 3
                                    },
                                    transform(char){
                                        return char
                                    }
                                },
                                "F": {
                                    validate(char){
                                        return (+char) < 6
                                    },
                                    transform(char){
                                        return char
                                    }
                                }
                            }}
                        />
                    </div>
                </div>
                <Checkbox
                    checked={!!this.state.repeat}
                    onChange={a => this.setState({repeat: a})}
                >
                    Повторять каждую неделю
                </Checkbox>
                <div
                    className="form-group"
                >
                    <label className="control-label">Дополнительная информация</label>
                    <textarea
                        placeholder="Кабиент №..."
                        className="form-control"
                        style={{maxWidth: '100%'}}
                        onChange={ev => {
                            this.setState({description: ev.target.value})
                        }}
                        value={this.state.description}
                    />
                </div>
                <div
                    className="form-group"
                >
                    <label className="control-label">Планируемое время на каждого пришедшего</label>
                    <input
                        className="form-control"
                        min={1} title="Минут на человека"
                        onBlur={e => {
                            e.preventDefault();
                            e.stopPropagation();
                            this.setState({avg_time_focus: false})
                        }}
                        type={this.state.avg_time_focus ? "number" : "text"}
                        onFocus={() => this.setState({avg_time_focus: true})}
                        onKeyUp={ev => this.setState({duration: parseInt(ev.target.value) || 0})}
                        onChange={ev => this.setState({duration: parseInt(ev.target.value) || 0})}
                        value={this.state.duration + (this.state.avg_time_focus ? 0 : RMinuts(' ' + ready.time.mins, this.state.duration))}
                    />
                </div>
                <div
                    className="form-group btn-group"
                >
                    <input type="submit" className="btn btn-success" value="Сохранить"/>
                    <button
                        onClick={(e) => {
                            e.preventDefault();
                            this.props.onCancel();
                        }} className="btn btn-danger"
                    >
                        Отменить
                    </button>
                </div>
            </form>
        )
    }
}

StreamCreateForm.contextTypes = {
    store: PropTypes.object
};


StreamCreateForm.propsTypes = {
    defaults: PropTypes.object,
    onSave: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired
};
export default StreamCreateForm;