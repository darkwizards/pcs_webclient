import React, {Component, PropTypes} from "react";
import Collapse from "react-bootstrap/lib/Fade";

export default class Notify extends Component {
    constructor(props) {

        super(props);

        this.state = {
            opened: true
        }

    }

    componentDidMount() {

        if (~this.props.notify.expire)
            setTimeout(() => {
                if (this.state.opened)
                    this.setState({opened: false})
            }, this.props.notify.expire)

    }

    render() {

        let {notify} = this.props;

        return (
            <Collapse
                transitionAppear={true}
                in={this.state.opened}
                onExited={() => {
                    this.setState({opened: false});
                    this.props.onHide({id: notify.id})
                }}
            >
                <div
                    title="Скрыть..."
                    className={"text-center col-xs-12 alert alert-" + ["info", "success", "warning", "danger"][notify.type]}
                    onClick={() => this.setState({opened: false})}
                >
                    {
                        notify.text
                    }
                </div>
            </Collapse>
        )
    }
}

Notify.contextTypes = {
    store: PropTypes.object
};

Notify.propsTypes = {
    notify: PropTypes.object.isRequired,
    onHide: PropTypes.func.isRequired
};