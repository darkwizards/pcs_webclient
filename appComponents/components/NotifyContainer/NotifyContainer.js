import React, {Component, PropTypes} from "react";
import Notify from "../../components/Notify/Notify";
import {connect} from "react-redux";
import {rmNotif} from "../../actions/notice";
import {bindActionCreators} from "redux";
import "./NotifyContainer.scss";

class NotifyContainer extends Component {
    constructor(props) {
        super(props)

    }


    render() {
        return (
            <div className="container notify-container">
                {
                    this.props.notice.slice(0, 3)
                        .map(notify =>
                            <Notify key={notify.id} onHide={search => this.props.rmNotif(search)}
                                    notify={notify}/>
                        )
                }
            </div>
        )
    }
}

NotifyContainer.contextTypes = {
    store: PropTypes.object
};

NotifyContainer.propsTypes = {
    rmNotif: PropTypes.func,
    notice: PropTypes.array
};

function mapStateToProps(state) {
    return {
        notice: state.notice,
        router: state.router
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        rmNotif
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NotifyContainer);