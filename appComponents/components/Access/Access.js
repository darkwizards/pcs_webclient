import React, {Component, PropTypes} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {goBack} from "redux-router";

class AccessTest extends Component {

    constructor(props) {

        super(props)

    }

    render() {

        if (this.props.profMask & this.props.mask)
            return this.props.children;

        else if (this.props.showMsg)
            return (
                <div className="col-md-push-2 col-md-8 text-center">
                    <h3 className="alert alert-danger">
                        У Вас недостаточно прав, чтобы увидеть эту страницу.
                    </h3>
                </div>
            );

        else
            return null

    }
}

AccessTest.defaultProps = {
    showMsg: false
};

AccessTest.contextTypes = {
    store: PropTypes.object
};

AccessTest.propsTypes = {
    mask: PropTypes.number.isRequired,
    showMsg: PropTypes.bool
};

function mapStateToProps(state) {
    return {
        profMask: state.session.user.perm
    };
}
function mapDispatchToProps(dispatch) {

    return bindActionCreators({goBack}, dispatch);

}

export default connect(mapStateToProps, mapDispatchToProps)(AccessTest);