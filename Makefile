SCRIPT_REP=<!-- script -->
STYLE_R=<!-- style -->

all: cleanFin webpack
	cp -r assets/* build/assets/
	cat ./lib/index.html | sed 's/$(SCRIPT_REP)/<script src=\x22\/assets\/bundle.js\x22><\/script>/g' | sed 's/$(STYLE_R)/<link rel=\x22stylesheet\x22 href=\x22\/assets\/bundle.css\x22\/>/g' > build/index.html
	cat ./lib/index.html | sed 's/$(SCRIPT_REP)/<script src=\x22\/assets\/admin.js\x22><\/script>/g' | sed 's/$(STYLE_R)/<link rel=\x22stylesheet\x22 href=\x22\/assets\/admin.css\x22\/>/g' > build/admin.html
	cat ./lib/index.html | sed 's/$(SCRIPT_REP)/<script src=\x22\/assets\/client_reg.js\x22><\/script>/g' | sed 's/$(STYLE_R)/<link rel=\x22stylesheet\x22 href=\x22\/assets\/client_reg.css\x22\/>/g' > build/client_reg.html
	notify-send -i emblem-default "Сборка клиентов VerQ успешно завершена"

cleanFin:
	rm -rf build **/stats.json

webpack:
	./node_modules/.bin/webpack

zip:
	(cd build && zip -r ../out.zip *)
	notify-send "Архивирование клиентов VerQ завершено"

debugEnvoy:
	(cd prods/Envoy ; ./bin/dev-server.js)

debugHermit:
	(cd prods/Hermit ; ./bin/dev-server.js)

debugRatmann:
	(cd prods/Ratmann ; ./bin/dev-server.js)
