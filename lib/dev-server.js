#!/usr/bin/env node
'use strict';

module.exports = function (prodLocation) {

    process.title = "NODE_DEV_SERVER";

    const
        WebpackDevServer = require('webpack-dev-server'),
        webpack = require('webpack'),
        config = require(process.env.PWD + '/webpack.config'),
        express = require('express'),
        proxy = require('proxy-middleware'),
        renderIndex = require('./render-index'),
        url = require('url'),

        indexMarkup = renderIndex(prodLocation),

        settings = require("../config/settings.json"),
        app = express();


    app.use(
        '/' + config.publicPath,
        proxy(url.parse('http://127.0.0.1:' + config.webpackPort + '/' + config.publicPath))
    );


    app.use("/api",
        // rout
        require("http-proxy-middleware")({
            target: settings.devApiServerHost,
            changeOrigin: true,
            pathRewrite(path){
                console.log(path);
                let a = url.parse(path);
                return a.pathname.slice(4) + ("?" + a.query || "") || "";
            }
        })
    );

    app.get('/*', function (req, res) {
        res.send(indexMarkup);
    });

    let server = new WebpackDevServer(webpack(config), {
        publicPath: config.output.publicPath,
        hot: true,
        stats: {colors: true}
    });

    server.listen(config.webpackPort, function (err) {
        if (err) {
            return console.log(err);
        }
        app.listen(config.port, function (err) {
            if (err) {
                return console.log(err);
            }
            console.log('Listening at http://0.0.0.0:%d', config.port);
        });
    });
};
