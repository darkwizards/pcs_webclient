'use strict';

let
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    fs = require('fs'),
    path = require('path'),
    webpack = require('webpack'),
    buildPath = 'build/assets',
    publicPath = 'assets',
    port = process.env.PORT || 3000,
    webpackPort = process.env.WEBPACK_PORT || 3001
    ;


module.exports = function (NAME) {
    return {
        devtool: "eval-source-map",
        entry: [
            "./app/index.js",
            'webpack/hot/only-dev-server',
            'webpack-dev-server/client?http://localhost:' + webpackPort
        ],
        output: {
            path: path.resolve(process.env.PWD, buildPath),
            filename: NAME + '.js',
            publicPath: 'http://localhost:' + webpackPort + '/' + publicPath + '/',
        },
        plugins: [
            new webpack.NoErrorsPlugin(),

            new webpack.IgnorePlugin(/vertx/),

            new webpack.HotModuleReplacementPlugin(),

            new ExtractTextPlugin(NAME + '.css', {allChunks: true}),

            new webpack.optimize.UglifyJsPlugin({
                sourceMap: false,
                mangle: false,
                drop_console: true,
                unsafe: true,
                warnings: false
            }),

            new webpack.optimize.DedupePlugin(),

            function () {
                this.plugin('done', function (stats) {
                    fs.writeFileSync(
                        path.resolve(process.env.PWD, 'stats.json'),
                        JSON.stringify(stats.toJson())
                    );
                });
            }
        ],
        resolve: {
            alias: {
                assets: path.resolve(process.env.PWD, '..', '..', 'assets')
            },
            extensions: ['', '.js']
        },
        module: {
            loaders: [
                {
                    test: /\.s?css$/,
                    loader: 'style!css!sass'
                },
                {
                    exclude: /node_modules/,
                    test: /\.js$/,
                    loaders: ['react-hot', 'babel']
                },
                {
                    test: /\.(jpg|png)$/,
                    loader: 'file'
                },
                {
                    test: /\.json$/,
                    loader: 'json'
                },
                {
                    test: /\.(eot|svg|ttf|woff|woff2)$/,
                    loader: 'file-loader?name=fonts/[name].[ext]'
                }
            ]
        },
        buildPath,
        publicPath,
        port: port,
        webpackPort: webpackPort,
        bail: false,
        watch: true,
    };
};