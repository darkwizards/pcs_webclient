'use strict';

const
    fs = require('fs'),
    path = require('path');

function renderIndex(name) {
    return fs.readFileSync(path.resolve(__dirname, 'index.html'))
        .toString()
        .replace('<!-- script -->', '<script src="/assets/' + name + '.js"></script>');
}

module.exports = renderIndex;
